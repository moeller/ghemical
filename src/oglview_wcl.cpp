// OGLVIEW_WCL.CPP

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "oglview_wcl.h"

#include <oglappth/oglv3d.h>
#include <oglappth/base_app.h>

#include "appdefine.h"
#include "custom_app.h"

/*################################################################################################*/

#define SB_SIZE 1024	// the selection buffer size.

bool oglview_wcl::quick_update = false;
bool oglview_wcl::draw_info = true;

oglview_wcl::oglview_wcl(custom_camera * cc) :
	pangofont_wcl(cc)
{
	ccam = cc;
	my_wnd_number = (ccam->wcl_counter++) + 1;
	
	render = RENDER_BALL_AND_STICK;
	
	label = LABEL_NOTHING;
	
	colormode = ccam->prj->GetDefaultColorMode();
	
	enable_fog = false;
	accumulate = false;
	
	select_buffer = new iGLu[SB_SIZE];
}

oglview_wcl::~oglview_wcl(void)
{
	delete[] select_buffer;
}

custom_camera * oglview_wcl::GetCCam(void)
{
	return ccam;	// this is the same as casting base_wcl::GetCam().
}

void oglview_wcl::GetCRD(i32s * p1, fGL * p2)
{
	base_wnd * w = GetWnd();
	
	if (w == NULL)	// just return zeros if not linked to any window...
	{
		p2[0] = p2[1] = p2[2] = 0.0;
		return;
	}
	
	oglv3d<GLfloat> xdir = (ccam->GetSafeLD()->ydir).vpr(ccam->GetSafeLD()->zdir); xdir = xdir / xdir.len();
	oglv3d<GLfloat> tmpv = oglv3d<GLfloat>(ccam->GetSafeLD()->crd); tmpv = tmpv + (ccam->GetSafeLD()->zdir * ccam->focus);
	tmpv = tmpv + xdir * (2.0 * vdim[0] * (fGL) (w->GetWidth() / 2 - p1[0]) / (fGL) w->GetWidth());
	tmpv = tmpv + ccam->GetSafeLD()->ydir * (2.0 * vdim[1] * (fGL) (w->GetHeight() / 2 - p1[1]) / (fGL) w->GetHeight());
	for (i32s n1 = 0;n1 < 3;n1++) p2[n1] = tmpv[n1];
}

static ogl_transformer tool_transformer;
static const ogl_obj_loc_data * tdata = NULL;
static const ogl_camera * rdata = NULL;

void oglview_wcl::ButtonEvent(int x, int y)
{
	if (GetWnd()->IsTimerON())
	{
		bool allow_anim = false;
		if (custom_app::GetCurrentMouseTool() == custom_app::mtZoom) allow_anim = true;
		if (custom_app::GetCurrentMouseTool() == custom_app::mtClipping) allow_anim = true;
		
		if (!allow_anim) GetWnd()->SetTimerOFF();
	}
	
	custom_transformer_client * ctc = custom_app::GetAppC()->GetPrj();
	
	static i32s stored_render;
	static bool stored_accumulate;
	
///////////////////////////////////////////////////////////////////////////
	
	if (custom_app::GetCurrentMouseTool() == custom_app::mtDraw)
	{
		mouseinfo::latest_x = x;
		mouseinfo::latest_y = y;
		
		MyUpdateWnd(pDraw, x, y);
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtErase)
	{
		mouseinfo::latest_x = x;
		mouseinfo::latest_y = y;
		
		MyUpdateWnd(pErase, x, y);
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtSelect)
	{
		if (mouseinfo::state != mouseinfo::sDown) return;
		
		MyUpdateWnd(pSelect, x, y);
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtZoom)
	{
		if (quick_update)
		{
			if (mouseinfo::state == mouseinfo::sDown)
			{
				stored_render = render;
				stored_accumulate = accumulate;
				
				render = RENDER_WIREFRAME;
				accumulate = false;
			}
			else
			{
				render = stored_render;
				accumulate = stored_accumulate;
				
				ccam->prj->UpdateGraphicsViews(ccam);
			}
		}
		
		mouseinfo::latest_x = x;
		mouseinfo::latest_y = y;
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtClipping)
	{
		if (quick_update)
		{
			if (mouseinfo::state == mouseinfo::sDown)
			{
				stored_render = render;
				stored_accumulate = accumulate;
				
				render = RENDER_WIREFRAME;
				accumulate = false;
			}
			else
			{
				render = stored_render;
				accumulate = stored_accumulate;
				
				ccam->prj->UpdateGraphicsViews(ccam);
			}
		}
		
		mouseinfo::latest_x = x;
		mouseinfo::latest_y = y;
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtTranslateXY)
	{
		if (mouseinfo::state == mouseinfo::sDown)
		{
			if (quick_update)		// if quick, change the rendering params...
			{
				stored_render = render;
				stored_accumulate = accumulate;
				
				render = RENDER_WIREFRAME;
				accumulate = false;
			}
			
			if (!mouseinfo::shift_down)
			{
				ctc->tc_object_ref = ccam;
				ctc->tc_local_object = true;
			}
			else
			{
				ctc->tc_object_ref = ccam->prj->selected_object;
				
				i32s index = base_app::GetAppB()->IsLight(ctc->tc_object_ref);
				ctc->tc_local_object = (index != NOT_DEFINED && dynamic_cast<ogl_light *>(ctc->tc_object_ref)->owner == ccam);
				
				if (!ctc->tc_object_ref && mouseinfo::shift_down)
				{
					tool_transformer.Init(custom_app::GetPrj());
					ctc->tc_object_ref = (& tool_transformer);
				}
				
				if (ctc->tc_object_ref) ctc->tc_object_ref->BeginTransformation();
			}
			
			// if CTRL is down, use object's own direction, not that of camera's !!!!!!!!!
			// if CTRL is down, use object's own direction, not that of camera's !!!!!!!!!
			// if CTRL is down, use object's own direction, not that of camera's !!!!!!!!!
			
			if (mouseinfo::ctrl_down) tdata = ctc->tc_object_ref->GetSafeLD();
			else tdata = ccam->GetSafeLD();
		}
		else
		{
			if (ctc->tc_object_ref) ctc->tc_object_ref->EndTransformation();
			
			if (quick_update)		// if quick, put back original params and update...
			{
				render = stored_render;
				accumulate = stored_accumulate;
				
				if (ctc->tc_local_object) ccam->prj->UpdateGraphicsViews(ccam);
				else ccam->prj->UpdateAllGraphicsViews();
			}
		}
		
		mouseinfo::latest_x = x;
		mouseinfo::latest_y = y;
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtTranslateZ)
	{
		if (mouseinfo::state == mouseinfo::sDown)
		{
			if (quick_update)		// if quick, change the rendering params...
			{
				stored_render = render;
				stored_accumulate = accumulate;
				
				render = RENDER_WIREFRAME;
				accumulate = false;
			}
			
			if (!mouseinfo::shift_down)
			{
				ctc->tc_object_ref = ccam;
				ctc->tc_local_object = true;
			}
			else
			{
				ctc->tc_object_ref = ccam->prj->selected_object;
				
				i32s index = base_app::GetAppB()->IsLight(ctc->tc_object_ref);
				ctc->tc_local_object = (index != NOT_DEFINED && dynamic_cast<ogl_light *>(ctc->tc_object_ref)->owner == ccam);
				
				if (!ctc->tc_object_ref && mouseinfo::shift_down)
				{
					tool_transformer.Init(ccam->prj);
					ctc->tc_object_ref = (& tool_transformer);
				}
				
				if (ctc->tc_object_ref) ctc->tc_object_ref->BeginTransformation();
			}
			
			// if CTRL is down, use object's own direction, not that of camera's !!!!!!!!!
			// if CTRL is down, use object's own direction, not that of camera's !!!!!!!!!
			// if CTRL is down, use object's own direction, not that of camera's !!!!!!!!!
			
			if (mouseinfo::ctrl_down) tdata = ctc->tc_object_ref->GetSafeLD();
			else tdata = ccam->GetSafeLD();
		}
		else
		{
			if (ctc->tc_object_ref) ctc->tc_object_ref->EndTransformation();
			
			if (quick_update)		// if quick, put back original params and update...
			{
				render = stored_render;
				accumulate = stored_accumulate;
				
				if (ctc->tc_local_object) ccam->prj->UpdateGraphicsViews(ccam);
				else ccam->prj->UpdateAllGraphicsViews();
			}
		}
		
		mouseinfo::latest_x = x;
		mouseinfo::latest_y = y;
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtOrbitXY)
	{
		if (mouseinfo::state == mouseinfo::sDown)
		{
			if (quick_update)		// if quick, change the rendering params...
			{
				stored_render = render;
				stored_accumulate = accumulate;
				
				render = RENDER_WIREFRAME;
				accumulate = false;
			}
			
			if (!mouseinfo::shift_down)
			{
				ctc->tc_object_ref = ccam;
				ctc->tc_local_object = true;
			}
			else
			{
				ctc->tc_object_ref = ccam->prj->selected_object;
				
				i32s index = base_app::GetAppB()->IsLight(ctc->tc_object_ref);
				ctc->tc_local_object = (index != NOT_DEFINED && dynamic_cast<ogl_light *>(ctc->tc_object_ref)->owner == ccam);
				
				if (!ctc->tc_object_ref && mouseinfo::shift_down)
				{
					tool_transformer.Init(ccam->prj);
					ctc->tc_object_ref = (& tool_transformer);
				}
				
				if (ctc->tc_object_ref) ctc->tc_object_ref->BeginTransformation();
			}
			
			rdata = ccam;
		}
		else
		{
			if (ctc->tc_object_ref) ctc->tc_object_ref->EndTransformation();
			
			if (quick_update)		// if quick, put back original params and update...
			{
				render = stored_render;
				accumulate = stored_accumulate;
				
				if (ctc->tc_local_object) ccam->prj->UpdateGraphicsViews(ccam);
				else ccam->prj->UpdateAllGraphicsViews();
			}
			
			// the animation switch is here...
			
			if (animX * animX + animY * animY > 7.5)
			{
				animX *= 0.25; animY *= 0.25;
				GetWnd()->SetTimerON(10);
			}
		}
		
		mouseinfo::latest_x = x;
		mouseinfo::latest_y = y;
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtOrbitZ)
	{
		if (mouseinfo::state == mouseinfo::sDown)
		{
			if (quick_update)		// if quick, change the rendering params...
			{
				stored_render = render;
				stored_accumulate = accumulate;
				
				render = RENDER_WIREFRAME;
				accumulate = false;
			}
			
			if (!mouseinfo::shift_down)
			{
				ctc->tc_object_ref = ccam;
				ctc->tc_local_object = true;
			}
			else
			{
				ctc->tc_object_ref = ccam->prj->selected_object;
				
				i32s index = base_app::GetAppB()->IsLight(ctc->tc_object_ref);
				ctc->tc_local_object = (index != NOT_DEFINED && dynamic_cast<ogl_light *>(ctc->tc_object_ref)->owner == ccam);
				
				if (!ctc->tc_object_ref && mouseinfo::shift_down)
				{
					tool_transformer.Init(ccam->prj);
					ctc->tc_object_ref = (& tool_transformer);
				}
				
				if (ctc->tc_object_ref) ctc->tc_object_ref->BeginTransformation();
			}
			
			rdata = ccam;
		}
		else
		{
			if (ctc->tc_object_ref) ctc->tc_object_ref->EndTransformation();
			
			if (quick_update)		// if quick, put back original params and update...
			{
				render = stored_render;
				accumulate = stored_accumulate;
				
				if (ctc->tc_local_object) ccam->prj->UpdateGraphicsViews(ccam);
				else ccam->prj->UpdateAllGraphicsViews();
			}
		}
		
		mouseinfo::latest_x = x;
		mouseinfo::latest_y = y;
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtRotateXY)
	{
		if (mouseinfo::state == mouseinfo::sDown)
		{
			if (quick_update)		// if quick, change the rendering params...
			{
				stored_render = render;
				stored_accumulate = accumulate;
				
				render = RENDER_WIREFRAME;
				accumulate = false;
			}
			
			if (!mouseinfo::shift_down)
			{
				ctc->tc_object_ref = ccam;
				ctc->tc_local_object = true;
			}
			else
			{
				ctc->tc_object_ref = ccam->prj->selected_object;
				
				i32s index = base_app::GetAppB()->IsLight(ctc->tc_object_ref);
				ctc->tc_local_object = (index != NOT_DEFINED && dynamic_cast<ogl_light *>(ctc->tc_object_ref)->owner == ccam);
				
				if (!ctc->tc_object_ref && mouseinfo::shift_down)
				{
					tool_transformer.Init(ccam->prj);
					ctc->tc_object_ref = (& tool_transformer);
				}
				
				if (ctc->tc_object_ref) ctc->tc_object_ref->BeginTransformation();
			}
			
			rdata = ccam;
		}
		else
		{
			if (ctc->tc_object_ref) ctc->tc_object_ref->EndTransformation();
			
			if (quick_update)		// if quick, put back original params and update...
			{
				render = stored_render;
				accumulate = stored_accumulate;
				
				if (ctc->tc_local_object) ccam->prj->UpdateGraphicsViews(ccam);
				else ccam->prj->UpdateAllGraphicsViews();
			}
		}
		
		mouseinfo::latest_x = x;
		mouseinfo::latest_y = y;
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtRotateZ)
	{
		if (mouseinfo::state == mouseinfo::sDown)
		{
			if (quick_update)		// if quick, change the rendering params...
			{
				stored_render = render;
				stored_accumulate = accumulate;
				
				render = RENDER_WIREFRAME;
				accumulate = false;
			}
			
			if (!mouseinfo::shift_down)
			{
				ctc->tc_object_ref = ccam;
				ctc->tc_local_object = true;
			}
			else
			{
				ctc->tc_object_ref = ccam->prj->selected_object;
				
				i32s index = base_app::GetAppB()->IsLight(ctc->tc_object_ref);
				ctc->tc_local_object = (index != NOT_DEFINED && dynamic_cast<ogl_light *>(ctc->tc_object_ref)->owner == ccam);
				
				if (!ctc->tc_object_ref && mouseinfo::shift_down)
				{
					tool_transformer.Init(ccam->prj);
					ctc->tc_object_ref = (& tool_transformer);
				}
				
				if (ctc->tc_object_ref) ctc->tc_object_ref->BeginTransformation();
			}
			
			rdata = ccam;
		}
		else
		{
			if (ctc->tc_object_ref) ctc->tc_object_ref->EndTransformation();
			
			if (quick_update)		// if quick, put back original params and update...
			{
				render = stored_render;
				accumulate = stored_accumulate;
				
				if (ctc->tc_local_object) ccam->prj->UpdateGraphicsViews(ccam);
				else ccam->prj->UpdateAllGraphicsViews();
			}
		}
		
		mouseinfo::latest_x = x;
		mouseinfo::latest_y = y;
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtMeasure)
	{
		if (mouseinfo::state != mouseinfo::sDown) return;
		
		MyUpdateWnd(pMeasure, x, y);
	}
}

void oglview_wcl::MotionEvent(int x, int y)
{
	custom_transformer_client * ctc = custom_app::GetAppC()->GetPrj();
	
///////////////////////////////////////////////////////////////////////////
	
	if (custom_app::GetCurrentMouseTool() == custom_app::mtDraw)
	{
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtErase)
	{
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtSelect)
	{
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtZoom)
	{
		fGL dist[3] = { 0.0, 0.0, 0.0 };
		dist[2] = mouseinfo::dist_sensitivity * vdim[1] * (fGL) (mouseinfo::latest_y - y) / (fGL) GetWnd()->GetHeight();
//cout << "AA : " << ccam->GetSafeLD()->crd[0] << " " << ccam->GetSafeLD()->crd[1] << " " << ccam->GetSafeLD()->crd[2] << endl;
		ccam->TranslateObject(dist, ccam->GetSafeLD());
//cout << "BB : " << ccam->GetSafeLD()->crd[0] << " " << ccam->GetSafeLD()->crd[1] << " " << ccam->GetSafeLD()->crd[2] << endl;
		ccam->focus += dist[2];
		
		if (quick_update) ccam->prj->UpdateGraphicsView(this);
		else ccam->prj->UpdateGraphicsViews(ccam);
		
		mouseinfo::latest_x = x;
		mouseinfo::latest_y = y;
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtClipping)
	{
		ccam->clipping += (fGL) (mouseinfo::latest_y - y) / (fGL) GetWnd()->GetHeight();
		if (ccam->clipping < 0.01) ccam->clipping = 0.01;
		if (ccam->clipping > 0.99) ccam->clipping = 0.99;
		
		cout << "clipping = " << ccam->clipping << " = ";
		cout << (ccam->clipping * (2.0 * ccam->focus)) << " nm." << endl;
		
		if (quick_update) ccam->prj->UpdateGraphicsView(this);
		else ccam->prj->UpdateGraphicsViews(ccam);
		
		mouseinfo::latest_x = x;
		mouseinfo::latest_y = y;
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtTranslateXY)
	{
		fGL dist[3] = { 0.0, 0.0, 0.0 };
		if (ctc->tc_object_ref != NULL && tdata != NULL)
		{
			dist[0] = mouseinfo::dist_sensitivity * vdim[0] * (fGL) (mouseinfo::latest_x - x) / (fGL) GetWnd()->GetWidth();
			dist[1] = mouseinfo::dist_sensitivity * vdim[1] * (fGL) (mouseinfo::latest_y - y) / (fGL) GetWnd()->GetHeight();
			ctc->tc_object_ref->TranslateObject(dist, tdata);
			
			if (quick_update) ccam->prj->UpdateGraphicsView(this);
			else
			{
				if (ctc->tc_local_object) ccam->prj->UpdateGraphicsViews(ccam);
				else ccam->prj->UpdateAllGraphicsViews();
			}
		}
		
		mouseinfo::latest_x = x;
		mouseinfo::latest_y = y;
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtTranslateZ)
	{
		fGL dist[3] = { 0.0, 0.0, 0.0 };
		if (ctc->tc_object_ref != NULL && tdata != NULL)
		{
			dist[2] = mouseinfo::dist_sensitivity * vdim[1] * (fGL) (mouseinfo::latest_y - y) / (fGL) GetWnd()->GetHeight();
			ctc->tc_object_ref->TranslateObject(dist, tdata);
			
			if (quick_update) ccam->prj->UpdateGraphicsView(this);
			else
			{
				if (ctc->tc_local_object) ccam->prj->UpdateGraphicsViews(ccam);
				else ccam->prj->UpdateAllGraphicsViews();
			}
		}
		
		mouseinfo::latest_x = x;
		mouseinfo::latest_y = y;
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtOrbitXY)
	{
		fGL ang[3] = { 0.0, 0.0, 0.0 };
		if (ctc->tc_object_ref != NULL)
		{
			ang[0] = mouseinfo::ang_sensitivity * (fGL) (mouseinfo::latest_y - y) / (fGL) GetWnd()->GetHeight();
			ang[1] = mouseinfo::ang_sensitivity * (fGL) (x - mouseinfo::latest_x) / (fGL) GetWnd()->GetWidth();
			ctc->tc_object_ref->OrbitObject(ang, * rdata);
			
			animX = ang[0]; animY = ang[1];
			
			if (quick_update) ccam->prj->UpdateGraphicsView(this);
			else
			{
				if (ctc->tc_local_object) ccam->prj->UpdateGraphicsViews(ccam);
				else ccam->prj->UpdateAllGraphicsViews();
			}
		}
		
		mouseinfo::latest_x = x;
		mouseinfo::latest_y = y;
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtOrbitZ)
	{
		fGL ang[3] = { 0.0, 0.0, 0.0 };
		if (ctc->tc_object_ref != NULL)
		{
			ang[2] = mouseinfo::ang_sensitivity * (fGL) (x - mouseinfo::latest_x) / (fGL) GetWnd()->GetWidth();
			ctc->tc_object_ref->OrbitObject(ang, * rdata);
			
			if (quick_update) ccam->prj->UpdateGraphicsView(this);
			else
			{
				if (ctc->tc_local_object) ccam->prj->UpdateGraphicsViews(ccam);
				else ccam->prj->UpdateAllGraphicsViews();
			}
		}
		
		mouseinfo::latest_x = x;
		mouseinfo::latest_y = y;
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtRotateXY)
	{
		fGL ang[3] = { 0.0, 0.0, 0.0 };
		if (ctc->tc_object_ref != NULL)
		{
			ang[0] = mouseinfo::ang_sensitivity * (fGL) (mouseinfo::latest_y - y) / (fGL) GetWnd()->GetHeight();
			ang[1] = mouseinfo::ang_sensitivity * (fGL) (x - mouseinfo::latest_x) / (fGL) GetWnd()->GetWidth();
			ctc->tc_object_ref->RotateObject(ang, * rdata);
			
			if (quick_update) ccam->prj->UpdateGraphicsView(this);
			else
			{
				if (ctc->tc_local_object) ccam->prj->UpdateGraphicsViews(ccam);
				else ccam->prj->UpdateAllGraphicsViews();
			}
		}
		
		mouseinfo::latest_x = x;
		mouseinfo::latest_y = y;
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtRotateZ)
	{
		fGL ang[3] = { 0.0, 0.0, 0.0 };
		if (ctc->tc_object_ref != NULL)
		{
			ang[2] = mouseinfo::ang_sensitivity * (fGL) (x - mouseinfo::latest_x) / (fGL) GetWnd()->GetWidth();
			ctc->tc_object_ref->RotateObject(ang, * rdata);
		
			if (quick_update) ccam->prj->UpdateGraphicsView(this);
			else
			{
				if (ctc->tc_local_object) ccam->prj->UpdateGraphicsViews(ccam);
				else ccam->prj->UpdateAllGraphicsViews();
			}
		}
		
		mouseinfo::latest_x = x;
		mouseinfo::latest_y = y;
	}
	
///////////////////////////////////////////////////////////////////////////

	if (custom_app::GetCurrentMouseTool() == custom_app::mtMeasure)
	{
	}
}

void oglview_wcl::UpdateWnd(void)
{
	base_wnd * w = GetWnd();
	if (w != NULL && ccam != NULL)
	{
		w->SetCurrent();
		MyUpdateWnd();
	}
	else cout << "DEBUG : oglview_wcl::UpdateWnd() : skipped!" << endl;
}

void oglview_wcl::MyUpdateWnd(pmode pm, int x, int y)
{
	if (GetWnd() == NULL) return;		// skip rendering if an invalid wnd!!!
	if (GetWnd()->GetWidth() < 0) return;	// skip rendering if an invalid wnd!!!
	
// A SIMPLE FRAMES-PER-SECOND COUNTER FOR GRAPHICS OPTIMIZATION
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
/*	const int numframes = 30;
	static int framecounter = 0;
	static double timecounter = 0.0;
	static long prevtime = 0;
	timeval tv; gettimeofday(& tv, NULL);
	long currtime = tv.tv_usec;		// according to manpages, this should be in microseconds???
	long difftime = currtime - prevtime;	// the CLOCKS_PER_SEC factor works a lot better than 1.0e-06
	prevtime = currtime;
	if (difftime > 0)	// skip the frame in timer overflow situation!!!
	{
		double t_secs = (double) difftime / (double) CLOCKS_PER_SEC;	// time unit???
		framecounter++; timecounter += t_secs;
		if (framecounter >= numframes)
		{
			cout << "frame rate = " << ((double) framecounter / timecounter) << " FPS   (";
			cout << framecounter << " frames in " << timecounter << " seconds)." << endl;
			framecounter = 0; timecounter = 0.0;
		}
	}	*/
// A SIMPLE FRAMES-PER-SECOND COUNTER FOR GRAPHICS OPTIMIZATION
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	bool pick = (pm != DoNotPick);
	cam->RenderScene(GetWnd(), accumulate, pick, x, y);
	
	// if this was a selection operation, read the information from the selection buffer.
	
	if (pick)
	{
		i32s tmp1 = glRenderMode(GL_RENDER);	// number of records in s-buffer.
		i32s tmp2 = NOT_DEFINED;		// index to the beginning of the selected record.
		
		iGLu tmp3 = 0xffffffff;		// original, lowest z-value...
	//	iGLu tmp3 = 0x00000000;		// alternative, highest z-value...
		
	/////////////////////////////////////////////////////////////////////////
	// here we examine the selection buffer and select the hit with lowest
	// z-value (since we assume that this object was visible to the user).
	/////////////////////////////////////////////////////////////////////////
		
		i32s tmp4[2] = { 0, 0 };
		while (tmp4[0] < tmp1)
		{
			if (select_buffer[tmp4[1] + 1] < tmp3)	// original, lowest z-value...
		//	if (select_buffer[tmp4[1] + 2] > tmp3)	// alternative, highest z-value...
			{
				tmp2 = tmp4[1];
				tmp3 = select_buffer[tmp4[1] + 1];	// original, lowest z-value...
			//	tmp3 = select_buffer[tmp4[1] + 2];	// alternative, highest z-value...
			}
			
			tmp4[0]++;
			tmp4[1] += select_buffer[tmp4[1]] + 3;
		}
		
		// now we copy all name records of the selected hit...
		
		vector<iGLu> name_vector;
		if (tmp2 != NOT_DEFINED)
		{
			for (i32u i1 = 0;i1 < select_buffer[tmp2];i1++)
			{
				name_vector.push_back(select_buffer[tmp2 + i1 + 3]);
			}
		}
		
		// "draw"- and "erase"-events are always forwarded to prj.
		
		if (pm == pDraw) ccam->prj->DrawEvent(this, name_vector);
		if (pm == pErase) ccam->prj->EraseEvent(this, name_vector);
		
		// "select"- and "measure"-events are forwarded only if
		// MODEL_DEPENDENT flag is set (an atom selection event).
		
		if (pm == pSelect && name_vector.size() > 1)
		{
			bool test = (name_vector[0] & GLNAME_MODEL_DEPENDENT);
			if (test)
			{
				ccam->prj->selected_object = NULL;
				ccam->prj->SelectEvent(this, name_vector);
			}
			else
			{
				// we are doing a light/object selection...
				// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
				
				base_app * app = base_app::GetAppB();
				custom_app * myapp = custom_app::GetAppC();
				
				const ogl_dummy_object * ref = NULL;
				switch (name_vector[0])
				{
					case GLNAME_LIGHT:
					ref = (const ogl_dummy_object *) app->FindPtrByGLName(name_vector[1]);
					myapp->SelectLight(ref);
					break;
					
					case GLNAME_OBJECT:
					ref = (const ogl_dummy_object *) app->FindPtrByGLName(name_vector[1]);
					myapp->GetPrj()->SelectObject(ref);
					break;
				}
			}
		}
		
		if (pm == pMeasure && name_vector.size() > 1)
		{
			bool test = (name_vector[0] & GLNAME_MODEL_DEPENDENT);
			if (test)
			{
				// todo : make measurement different from
				// selection (do not use a selection flag but
				// make a separate way to show it) -> no need
				// to mess with selected_object here anymore...
				
			ccam->prj->selected_object = NULL;	// FixMe!!!
				
				ccam->prj->MeasureEvent(this, name_vector);
			}
		}
	}
}

void oglview_wcl::InitGL(void)
{
	const fGL background[4] = { 0.0, 0.0, 0.0, 1.0};
	glClearColor(background[0], background[1], background[2], background[3]);
	
	glDepthFunc(GL_LESS); glEnable(GL_DEPTH_TEST);
	glMateriali(GL_FRONT_AND_BACK, GL_SHININESS, 64);
	
	const fGL specular_reflectance[4] = { 0.9, 0.9, 0.9, 1.0 };
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular_reflectance);
	
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);
	
	const fGL ambient_intensity[4] = { 0.2, 0.2, 0.2, 1.0 };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient_intensity);
	
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, true);
	
	glFogi(GL_FOG_MODE, GL_EXP);
	glFogf(GL_FOG_DENSITY, 0.15);
	
	const fGL fog_color[4] = { 0.0, 0.0, 0.0, 0.0 };
	glFogfv(GL_FOG_COLOR, fog_color);
	
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	glSelectBuffer(SB_SIZE, select_buffer);
	
	// also setup the lights, just to make sure it always happens...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	base_app::GetAppB()->SetupLights(ccam);
	
	// all classes that inherit pangofont_wcl must call ogl_InitPangoFont()!!!
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	ogl_InitPangoFont("courier 12");
}

void oglview_wcl::RenderGL(rmode rm)
{
	custom_app::GetAppC()->GetPrj()->Render(this, rm);
}

/*################################################################################################*/

// eof
