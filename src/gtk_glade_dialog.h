// GTK_GLADE_DIALOG.H : write a short description here...

// Copyright (C) 2002 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#ifndef GTK_GLADE_DIALOG_H
#define GTK_GLADE_DIALOG_H

//#include "ghemicalconfig2.h"

#include <glade/glade.h>

/*################################################################################################*/

class gtk_glade_dialog
{
	protected:
	
	GladeXML * xml;
	
	public:
	
	gtk_glade_dialog(const char *);
	virtual ~gtk_glade_dialog(void);
};

/*################################################################################################*/

#endif	// GTK_GLADE_DIALOG_H

// eof
