// CUSTOM_LIGHTS.H : extended ogl_light classes.

// Copyright (C) 2006 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#ifndef CUSTOM_LIGHTS_H
#define CUSTOM_LIGHTS_H

class rendered_spot_light;

/*################################################################################################*/

#include <ghemical/typedef.h>
#include <oglappth/ogl_lights.h>

/*################################################################################################*/

class rendered_spot_light :
	public ogl_spot_light
{
	protected:
	
	iGLu my_glname;
	
	public:
	
	rendered_spot_light(const ogl_object_location &, const ogl_light_components & = ogl_light::def_components, GLfloat = ogl_spot_light::def_cutoff, GLfloat = ogl_spot_light::def_exponent);
	virtual ~rendered_spot_light(void);
	
	void Render(void);	// virtual
};

/*################################################################################################*/

#endif	// CUSTOM_LIGHTS_H

// eof
