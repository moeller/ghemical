// OGL_PLANE.H : a classes for drawing colored 3D-planes and doing volume rendering.

// Copyright (C) 1998 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#ifndef OGL_PLANE_H
#define OGL_PLANE_H

//#include "ghemicalconfig2.h"

struct ogl_cp_param;
class ogl_color_plane;

class ogl_color_plane_object;
class ogl_volume_rendering_object;

/*################################################################################################*/

#include <oglappth/ogl_camera.h>
#include <oglappth/transparent.h>

#include <ghemical/typedef.h>

#include "project.h"

/*################################################################################################*/

struct ogl_cp_param
{
	project * prj;
	const ogl_obj_loc_data * data;	// for color_plane only!!!
	
	iGLu my_glname;			// for color_plane only!!!
	
	bool transparent;
	bool automatic_cv2;
	
	i32s np; fGL dim;
	
	engine * ref;
	ValueFunction * vf;
	ColorFunction * cf;
	
	fGL cvalue1;
	fGL cvalue2;
	fGL alpha;
};

class ogl_color_plane
{
	private:
	
	project * prj;
	const ogl_obj_loc_data * data;
	
	iGLu my_glname;
	
	bool transparent;
	bool automatic_cv2;
	
	i32s np; fGL dim;
	
	engine * ref;
	ValueFunction * GetValue;
	ColorFunction * GetColor;
	
	fGL cvalue1;
	fGL cvalue2;
	fGL alpha;
	
/*################*/
/*################*/
	
	fGL * dist;
	
	fGL * color4;
	fGL * point3;
	
	oglv3d<GLfloat> xdir;
	
	public:
	
	ogl_color_plane(ogl_cp_param &);
	~ogl_color_plane(void);
	
	void Update(void);
	void Render(void);
	
	protected:
	
	void SetDimension(fGL);
	void GetCRD(i32s *, fGL *);
};

/*################################################################################################*/

class ogl_color_plane_object :
	public ogl_smart_object
{
	private:
	
	ogl_color_plane * cp;
	
	engine * copy_of_ref;
	char * object_name;
	
	public:
	
	ogl_color_plane_object(const ogl_object_location &, ogl_cp_param &, const char *);
	~ogl_color_plane_object(void);
	
	engine * GetRef(void) { return copy_of_ref; }
	const char * GetObjectName(void) { return object_name; }	// virtual
	
	void CameraEvent(const ogl_camera &) { }	// virtual
	
	bool BeginTransformation(void) { return false; }	// virtual
	bool EndTransformation(void) { return false; }		// virtual
	
	void OrbitObject(const fGL *, const ogl_camera &);		// virtual
	void RotateObject(const fGL *, const ogl_camera &);		// virtual
	
	void TranslateObject(const fGL *, const ogl_obj_loc_data *);	// virtual
	
	void Render(void) { cp->Render(); }	// virtual
	void Update(void) { cp->Update(); }
};

/*################################################################################################*/

class ogl_volume_rendering_object :
	public ogl_smart_object
{
	private:
	
	fGL thickness;
	vector<ogl_color_plane *> cp_vector;
	vector<ogl_obj_loc_data *> data_vector;
	
	void * copy_of_ref;
	char * object_name;
	
	public:
	
	ogl_volume_rendering_object(const ogl_object_location &, ogl_cp_param &, i32s, fGL, ogl_camera &, const char *);
	~ogl_volume_rendering_object(void);
	
	void * GetRef(void) { return copy_of_ref; }
	const char * GetObjectName(void) { return object_name; }	// virtual

	void CameraEvent(const ogl_camera &);	// virtual
	
	bool BeginTransformation(void) { return false; }	// virtual
	bool EndTransformation(void) { return false; }		// virtual
	
	void OrbitObject(const fGL *, const ogl_camera &);		// virtual
	void RotateObject(const fGL *, const ogl_camera &);		// virtual
	
	void TranslateObject(const fGL *, const ogl_obj_loc_data *);	// virtual
	
	void Render(void);	// virtual
	void Update(void);
};

/*################################################################################################*/

#endif	// OGL_PLANE_H

// eof
