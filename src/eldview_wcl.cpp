// ELDVIEW_WCL.CPP

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "eldview_wcl.h"

#include "custom_app.h"

#include "local_i18n.h"

#include <GL/gl.h>
#include <GL/glu.h>

#include <sstream>
using namespace std;

/*################################################################################################*/

eldview_wcl::eldview_wcl(void) :
	pangofont_wcl(new ogl_camera(ogl_ol_static(), 1.0))
{
	cam->ortho = true;
	
	cam->update_vdim = false;
	vdim[0] = 0.75;			// X-scaling
	
	cam->GetLD()->crd[0] = 0.65;	// X-centering
	
//const ogl_obj_loc_data * d = cam->GetSafeLD();
//cout << "cam crd = " << d->crd[0] << " " << d->crd[1] << " " << d->crd[2] << endl;
//cout << "cam zdir = { " << d->zdir << " }" << endl << "cam ydir = { " << d->ydir << " }" << endl;
	
	reset_needed = true;
	SetCenterAndScale();
}

eldview_wcl::~eldview_wcl(void)
{
	// problem : lifetime of the camera object needs to be longer than
	// lifetime of this object since it is needed at the base class dtor.
	// solution : ask the base class to do the cleanup work for us...
	
	delete_cam_plz = true;
}

void eldview_wcl::SetCenterAndScale(void)
{
	project * prj = custom_app::GetAppC()->GetPrj();
	engine * eng = prj->GetCurrentSetup()->GetCurrentEngine();
	
	setup1_qm * su1 = dynamic_cast<setup1_qm *>(prj->GetCurrentSetup());
	bool nonQM = (!su1);
	
	if (!eng || nonQM)
	{
		cam->GetLD()->crd[1] = 0.0;			// center
		vdim[1] = 1.0;					// scale
		
		reset_needed = true;		// failed...
	}
	else
	{
		f64 mine = eng->GetOrbitalEnergy(0);
		f64 maxe = eng->GetOrbitalEnergy(eng->GetOrbitalCount() - 1);
		
// above we assumed that the first orbital has lowest and the last orbital has highest energy...
// above we assumed that the first orbital has lowest and the last orbital has highest energy...
// above we assumed that the first orbital has lowest and the last orbital has highest energy...
		
		cam->GetLD()->crd[1] = (mine + maxe) / 2.0;	// center
		vdim[1] = (maxe - mine) * 0.75;			// scale
		
		reset_needed = false;		// success!!!
	}
}

void eldview_wcl::ButtonEvent(int x, int y)
{
	mouseinfo::latest_x = x;
	mouseinfo::latest_y = y;
}

void eldview_wcl::MotionEvent(int x, int y)
{
	int dy = mouseinfo::latest_y - y;
	
	if (custom_app::GetCurrentMouseTool() == custom_app::mtZoom)
	{
		vdim[1] += mouseinfo::dist_sensitivity * vdim[1] * (float) dy / (float) GetWnd()->GetHeight();
		GetWnd()->RequestUpdate(false);
	}
	
	if (custom_app::GetCurrentMouseTool() == custom_app::mtTranslateXY)
	{
		cam->GetLD()->crd[1] -= 2.0 * vdim[1] * (float) dy / (float) GetWnd()->GetHeight();
		GetWnd()->RequestUpdate(false);
	}
	
	mouseinfo::latest_x = x;
	mouseinfo::latest_y = y;
}

void eldview_wcl::UpdateWnd(void)
{
	base_wnd * wnd = GetWnd();
	if (!wnd || wnd->GetWidth() < 0 || !cam) return;
	
	if (reset_needed) SetCenterAndScale();
	
	wnd->SetCurrent();
	cam->RenderScene(wnd, false, false);
}

void eldview_wcl::InitGL(void)
{
	// all classes that inherit pangofont_wcl must call ogl_InitPangoFont()!!!
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	ogl_InitPangoFont("courier 12");
}

void eldview_wcl::RenderGL(rmode)
{
	base_wnd * wnd = GetWnd();
	if (!wnd || wnd->GetWidth() < 0 || !cam) return;
	
	const GLfloat label_color[3] = { 1.0, 1.0, 1.0 };
	
	glInitNames();
	
	project * prj = custom_app::GetAppC()->GetPrj();
	engine * eng = prj->GetCurrentSetup()->GetCurrentEngine();
	
	setup1_qm * su1 = dynamic_cast<setup1_qm *>(prj->GetCurrentSetup());
	bool nonQM = (!su1);
	
	if (!eng || nonQM)
	{
		reset_needed = true;
		
		glColor3f(label_color[0], label_color[1], label_color[2]);
		
		GLfloat xpos;
		GLfloat ypos;
		
		const char * txt1 = _("No data available,");
		xpos = (wnd->GetWidth() - ogl_GetStringWidth(txt1)) / 2.0;
		ypos = (wnd->GetHeight() - 24) / 2.0 + 14;
		ogl_WriteString2D(txt1, xpos, ypos);
		
		const char * txt2 = (nonQM ? _("must be a QM model!") : _("please calculate energy!"));
		xpos = (wnd->GetWidth() - ogl_GetStringWidth(txt2)) / 2.0;
		ypos = (wnd->GetHeight() - 24) / 2.0 - 14;
		ogl_WriteString2D(txt2, xpos, ypos);
	}
	else
	{
		const GLfloat fontsize = 10.0;
		
		const GLfloat center = cam->GetLD()->crd[1];
		const GLfloat scale = vdim[1];
		
		const GLfloat one_pixel_height = scale / (GLfloat) wnd->GetHeight();
		
		const GLfloat diagram_line_thickness = 2.0;
		const GLfloat diagram_triangle_height = 45.0;
		const GLfloat diagram_triangle_width = 0.075;
		
// the functions: GetOrbitalCount(), GetOrbitalEnergy(), need to separate between
// alpha and beta electrons. also a simple way to determine occupation (and whether
// is alpha/beta) is needed. THIS PLOT IS FOR RHF SYSTEMS ONLY!!!!!!!!!!!!!!!!!!!!
		
		// gather the degenerate energy levels (del) into groups...
		
		const GLfloat del_tolerance = 0.001;
		int * del_first = new int[eng->GetOrbitalCount()];
		
		del_first[0] = 0; int dd = 1;
		while (dd < eng->GetOrbitalCount())
		{
			int ind_first = del_first[dd - 1];
			
			GLfloat ene_first = eng->GetOrbitalEnergy(ind_first);
			GLfloat ene_current = eng->GetOrbitalEnergy(dd);
			
			if (ene_current - ene_first < del_tolerance)
			{
				del_first[dd] = ind_first;	// belongs to the previous set.
			}
			else
			{
				del_first[dd] = dd;		// is the first of a new set.
			}
			
			dd++;
		}
		
		int * del_count = new int[eng->GetOrbitalCount()];
		for (dd = 0;dd < eng->GetOrbitalCount();dd++) del_count[dd] = 0;
		
		dd = 0; int first = 0;
		while (dd < eng->GetOrbitalCount())	// calculate the group sizes...
		{
			if (del_first[dd] != del_first[first]) first = dd;
			del_count[first]++;
			dd++;
		}
		
		dd = 0; first = 0;
		while (dd < eng->GetOrbitalCount())	// ...and copy them to all group members.
		{
			if (del_first[dd] != del_first[first]) first = dd;
			if (first != dd) del_count[dd] = del_count[first];
			dd++;
		}
		
		// render...
		
		for (int n1 = 0;n1 < eng->GetOrbitalCount();n1++)
		{
			GLfloat energy = eng->GetOrbitalEnergy(n1);
			
			GLfloat width = 1.0 / (fGL) del_count[n1];
			GLfloat left = 0.0 + (fGL) (n1 - del_first[n1]) * width;
			GLfloat right = 0.0 + (fGL) (n1 - del_first[n1] + 1) * width;
			
			// draw the line...
			// draw the line...
			// draw the line...
			
			glColor3f(0.0, 1.0, 0.0);	// green
			glBegin(GL_QUADS);
			
			glVertex3f(right - 0.05 * width, energy, 0.1);
			glVertex3f(left + 0.05 * width, energy, 0.1);
			glVertex3f(left + 0.05 * width, energy + one_pixel_height * diagram_line_thickness, 0.1);
			glVertex3f(right - 0.05 * width, energy + one_pixel_height * diagram_line_thickness, 0.1);
			
			glEnd();
			
			// draw the electrons...
			// draw the electrons...
			// draw the electrons...
			
			if (n1 < eng->GetElectronCount() / 2)
			{
				glColor3f(1.0, 1.0, 0.0);	// yellow
				glBegin(GL_TRIANGLES);
				
glVertex3f(left + 0.3 * width - diagram_triangle_width, energy - one_pixel_height * diagram_triangle_height / 3.0, 0.0);
glVertex3f(left + 0.3 * width, energy + one_pixel_height * diagram_triangle_height / 1.5, 0.0);
glVertex3f(left + 0.3 * width + diagram_triangle_width, energy - one_pixel_height * diagram_triangle_height / 3.0, 0.0);

glVertex3f(right - 0.3 * width - diagram_triangle_width, energy + one_pixel_height * diagram_triangle_height / 3.0, 0.0);
glVertex3f(right - 0.3 * width, energy - one_pixel_height * diagram_triangle_height / 1.5, 0.0);
glVertex3f(right - 0.3 * width + diagram_triangle_width, energy + one_pixel_height * diagram_triangle_height / 3.0, 0.0);

				glEnd();
			}
			
			// print out the text...
			// print out the text...
			// print out the text...
			
			glColor3f(label_color[0], label_color[1], label_color[2]);
			
			GLfloat xpos;
			GLfloat ypos;
			
			ostringstream str;
			str << "i = " << n1 << " e = " << energy << ends;
			
			xpos = 10.0;
			ypos = (0.5 + (energy - center) / (scale * 2.0)) * wnd->GetHeight() - fontsize / 2.0;
			ypos += (fGL) (n1 - del_first[n1]) * fontsize;
			
			ogl_WriteString2D(str.str().c_str(), xpos, ypos);
		}
		
		delete[] del_first;
		delete[] del_count;
	}
}

/*################################################################################################*/

// eof
