// GTK_APP.H : a GTK2 application class.

// Copyright (C) 2003 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#ifndef GTK_APP_H
#define GTK_APP_H

#include "ghemicalconfig2.h"

struct pv_views_objs_record;
struct pv_chains_record;
struct pv_atoms_record;
struct pv_bonds_record;

#include <glib.h>
#include <unistd.h>

#include "custom_app.h"
#include "gtk_project.h"

// uncomment this if you wish to have the traditional short toolbar labels.
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#define USE_SHORT_TOOLBAR_LABELS

/*################################################################################################*/

struct pv_viewsobjs_record
{
	custom_camera * owner;		///< this is non-NULL for child items.
	
	ogl_dummy_object * refV1;	///< for camera/light-type objects.
	base_wcl * refV2;		///< for view/client-type objects.
	
	ogl_smart_object * refO;	///< for object-type objects.
	
	GtkTreeIter iter;
};

struct pv_chains_record
{
	i32s c_r_ind;
	GtkTreeIter iter;
};

struct pv_atoms_record
{
	atom * ref;
	GtkTreeIter iter;
};

struct pv_bonds_record
{
	bond * ref;
	GtkTreeIter iter;
};

class gtk_app :
	public custom_app
{
	protected:
	
	static GtkActionEntry entries1[];
	static GtkToggleActionEntry entries2[];
	static const char * ui_description;
	
	static GtkUIManager * ui_manager;
	
	static GtkWidget * main_window;
	static GtkWidget * main_vbox;
	
	static GtkWidget * main_menubar;
	static GtkWidget * main_toolbar;
	
static GtkWidget * mtb_mtool_draw;
static GtkWidget * mtb_mtool_erase;
static GtkWidget * mtb_mtool_select;
static GtkWidget * mtb_mtool_zoom;
static GtkWidget * mtb_mtool_clipping;
static GtkWidget * mtb_mtool_translate_xy;
static GtkWidget * mtb_mtool_translate_z;
static GtkWidget * mtb_mtool_orbit_xy;
static GtkWidget * mtb_mtool_orbit_z;
static GtkWidget * mtb_mtool_rotate_xy;
static GtkWidget * mtb_mtool_rotate_z;
static GtkWidget * mtb_mtool_measure;
	
	// here is the former gtk_project stuff...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	static GtkWidget * paned_widget;
	
	static GtkWidget * notebook_widget;
	
	static GtkTextBuffer * txt_buffer;
	static GtkTextMark * end_mark;
	
	static GtkWidget * scroll_widget;
	static GtkWidget * txt_widget;
	
	// here is the former gtk_project_view stuff...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	static GtkActionEntry pv_viewsobjs_entries[];
	static const char * pv_viewsobjs_ui_description;
	
	static GtkActionEntry pv_chains_entries[];
	static const char * pv_chains_ui_description;
	
	static GtkActionEntry pv_atoms_entries[];
	static const char * pv_atoms_ui_description;
	
	static GtkActionEntry pv_bonds_entries[];
	static const char * pv_bonds_ui_description;
	
	static GtkWidget * pv_view_widget;
	static GtkWidget * pv_label_widget;
	
	list<pv_viewsobjs_record *> pv_viewsobjs_data;
	GtkTreeStore * pv_viewsobjs_store;
	GtkWidget * pv_viewsobjs_widget;
	GtkWidget * pv_viewsobjs_label;
	GtkWidget * pv_viewsobjs_menu;
	GtkWidget * pv_viewsobjs_sw;
	
	list<pv_chains_record *> pv_chains_data;
	GtkTreeStore * pv_chains_store;
	GtkWidget * pv_chains_widget;
	GtkWidget * pv_chains_label;
	GtkWidget * pv_chains_menu;
	GtkWidget * pv_chains_sw;
	
	list<pv_atoms_record *> pv_atoms_data;
	GtkListStore * pv_atoms_store;
	GtkWidget * pv_atoms_widget;
	GtkWidget * pv_atoms_label;
	GtkWidget * pv_atoms_menu;
	GtkWidget * pv_atoms_sw;
	
	list<pv_bonds_record *> pv_bonds_data;
	GtkListStore * pv_bonds_store;
	GtkWidget * pv_bonds_widget;
	GtkWidget * pv_bonds_label;
	GtkWidget * pv_bonds_menu;
	GtkWidget * pv_bonds_sw;
	
// friends...
// ^^^^^^^^^^
	friend class gtk_file_open_dialog;
	
	private:
	
// the constructor is private so that only a single instance
// of the class can be created (by calling GetGtkApp()).
	
	gtk_app(void);
	
	public:
	
	~gtk_app(void);
	
	static gtk_app * GetAppX(void);
	static gtk_project * GetPrjX(void);
	
	void AttachDetachView(base_wcl *);
	void SetTransientForMainWnd(GtkWindow *);
	
	void AddTabToNB(GtkWidget *, GtkWidget *);
	void RemoveTabFromNB(GtkWidget *);
	
	void SetTabTitleNB(GtkWidget *, GtkWidget *);
	
	protected:
	
	void InitPV(void);
	
	void SetNewProject(void);	// virtual
	
	static gboolean DeleteEventHandler(GtkWidget *, GdkEvent *, gpointer);
	static void DestroyHandler(GtkWidget *, gpointer);
	
	public:
	
	static void sMessage(const char *);
	static void sWarningMessage(const char *);
	static void sErrorMessage(const char *);
	
	static bool sQuestion(const char *);
	static void sPrintToLog(const char *);
	
	void Message(const char *);		// virtual
	void WarningMessage(const char *);	// virtual
	void ErrorMessage(const char *);	// virtual
	
	bool Question(const char *);		// virtual
	void PrintToLog(const char *);		// virtual
	
	static GtkUIManager * GetUIManager(void) { return ui_manager; }
	static GtkWidget * GetMainWindow(void) { return main_window; }
	
	void UpdateAllWindowTitles(void);	// virtual
	
	void CameraAdded(custom_camera *);		// virtual
	void CameraRemoved(custom_camera *);		// virtual
	
	void LightAdded(ogl_light *);			// virtual
	void LightRemoved(ogl_light *);			// virtual
	
	void GraphicsClientAdded(oglview_wcl *);	// virtual
	void GraphicsClientRemoved(oglview_wcl *);	// virtual
	
	void PlottingClientAdded(base_wcl *);		// virtual
	void PlottingClientRemoved(base_wcl *);		// virtual
	
	void ObjectAdded(ogl_smart_object *);		// virtual
	void ObjectRemoved(ogl_smart_object *);		// virtual
	
	static gint ViewsObjsPopupHandler(GtkWidget *, GdkEvent *);
	static void viewsobjs_SetCurrent(GtkWidget *, gpointer);
	static void viewsobjs_Delete(GtkWidget *, gpointer);
	
	void BuildChainsView(void);			// virtual
	void ClearChainsView(void);			// virtual
	static gint ChainsPopupHandler(GtkWidget *, GdkEvent *);
	static void chains_UpdateView(GtkWidget *, gpointer);
	static void chains_SelectItem(GtkWidget *, gpointer);
	
	void AtomAdded(atom *);				// virtual
	void AtomUpdateItem(atom *);			// virtual
	void AtomRemoved(atom *);			// virtual
	static gint AtomsPopupHandler(GtkWidget *, GdkEvent *);
	static void atoms_SelectAtom(GtkWidget *, gpointer);
	static gint atoms_ToggleLocked(GtkWidget *, gchar *, gpointer *);
	
	void BondAdded(bond *);				// virtual
	void BondUpdateItem(bond *);			// virtual
	void BondRemoved(bond *);			// virtual
	static gint BondsPopupHandler(GtkWidget *, GdkEvent *);
	static void bonds_SelectBond(GtkWidget *, gpointer);
	
// the toolbar-button callbacks start here ; the toolbar-button callbacks start here
// the toolbar-button callbacks start here ; the toolbar-button callbacks start here
// the toolbar-button callbacks start here ; the toolbar-button callbacks start here
	
	static void HandleToggleButtons(custom_app::mtool);
	static void maintb_tool_Draw(gpointer, guint, GtkWidget *);
	static void maintb_tool_Erase(gpointer, guint, GtkWidget *);
	static void maintb_tool_Select(gpointer, guint, GtkWidget *);
	static void maintb_tool_Zoom(gpointer, guint, GtkWidget *);
	static void maintb_tool_Clipping(gpointer, guint, GtkWidget *);
	static void maintb_tool_TranslateXY(gpointer, guint, GtkWidget *);
	static void maintb_tool_TranslateZ(gpointer, guint, GtkWidget *);
	static void maintb_tool_OrbitXY(gpointer, guint, GtkWidget *);
	static void maintb_tool_OrbitZ(gpointer, guint, GtkWidget *);
	static void maintb_tool_RotateXY(gpointer, guint, GtkWidget *);
	static void maintb_tool_RotateZ(gpointer, guint, GtkWidget *);
	static void maintb_tool_Measure(gpointer, guint, GtkWidget *);
	
	static void maintb_dial_Element(gpointer, guint, GtkWidget *);
	static void maintb_dial_BondType(gpointer, guint, GtkWidget *);
	static void maintb_dial_Setup(gpointer, guint, GtkWidget *);
	
// the main-menu callbacks start here ; the main-menu callbacks start here
// the main-menu callbacks start here ; the main-menu callbacks start here
// the main-menu callbacks start here ; the main-menu callbacks start here
	
	static void mainmenu_FileNew(gpointer, guint, GtkWidget *);
	static void mainmenu_FileOpen(gpointer, guint, GtkWidget *);
	static void mainmenu_FileSaveAs(gpointer, guint, GtkWidget *);
	static void mainmenu_FileClose(gpointer, guint, GtkWidget *);
	
	static void mainmenu_HelpHelp(gpointer, guint, GtkWidget *);
	static void mainmenu_HelpAbout(gpointer, guint, GtkWidget *);
};

/*################################################################################################*/

#endif	// GTK_APP_H

// eof
