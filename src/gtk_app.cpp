// GTK_APP.CPP

// Copyright (C) 2003 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "gtk_app.h"

#include <ghemical/notice.h>

#include "gtk_setup_dialog.h"

#include <ghemical/utility.h>

#ifndef WIN32
#include <X11/Xlib.h>	// DisplayString()
#endif	// WIN32

#include "res_gtk/draw.xpm"
#include "res_gtk/erase.xpm"
#include "res_gtk/select.xpm"
#include "res_gtk/zoom.xpm"
#include "res_gtk/clipping.xpm"
#include "res_gtk/transl_xy.xpm"
#include "res_gtk/transl_z.xpm"
#include "res_gtk/orbit_xy.xpm"
#include "res_gtk/orbit_z.xpm"
#include "res_gtk/rotate_xy.xpm"
#include "res_gtk/rotate_z.xpm"
#include "res_gtk/measure.xpm"

#include "res_gtk/element.xpm"
#include "res_gtk/bondtype.xpm"
#include "res_gtk/setup.xpm"

#include "gtk_wnd.h"

#include "local_i18n.h"

#include <cstring>
#include <sstream>
using namespace std;

/*################################################################################################*/

const char * tb_mousetool_labels[] =
#ifdef USE_SHORT_TOOLBAR_LABELS
// http://library.gnome.org/devel/glib/stable/glib-I18N.html
// what about using Q_() ; tbl_s|d = toolbarlabel_short_d ??? need to cut away the prefix?
// 2009-04-16 ; THERE IS NO NEED TO TRANSLATE THESE AT ALL ; just use the translated tooltips...
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
{
	"d",		"e",		"s",		"z",		"c",
	"t",		"tz",		"o",		"oz",		"r",
	"rz",		"m",		NULL
};
#else	// USE_SHORT_TOOLBAR_LABELS
{
	"draw\n",	"erase\n",	"select\n",	"zoom\n",	"clip\n",
	"trans\nXY",	"trans\nZ",	"orbit\nXY",	"orbit\nZ",	"rotate\nXY",
	"rotate\nZ",	"measure\n",	NULL
};
#endif	// USE_SHORT_TOOLBAR_LABELS

const char * tb_shortcut_labels[] =
#ifdef USE_SHORT_TOOLBAR_LABELS
{
	"el",		"bt",		"su",		NULL
};
#else	// USE_SHORT_TOOLBAR_LABELS
{
	"element\n",	"bond\ntype",	"setup\n",	NULL
};
#endif	// USE_SHORT_TOOLBAR_LABELS

GtkActionEntry gtk_app::entries1[] =
{
	{ "FileMenu", NULL, N_("File") },
	{ "HelpMenu", NULL, N_("Help") },
	
	{ "New", GTK_STOCK_NEW, N_("New"), NULL, N_("Create a new file."), (GCallback) gtk_app::mainmenu_FileNew },		//<control>N
	{ "Open", GTK_STOCK_OPEN, N_("Open"), NULL, N_("Open an existing file."), (GCallback) gtk_app::mainmenu_FileOpen },	//<control>O
	{ "SaveAs", GTK_STOCK_OPEN, N_("Save as..."), NULL, N_("Save a file."), (GCallback) gtk_app::mainmenu_FileSaveAs },	//<control>S
	{ "Close", GTK_STOCK_QUIT, N_("Close"), NULL, N_("Quit the program."), (GCallback) gtk_app::mainmenu_FileClose },	//<control>C
	
	{ "Help", GTK_STOCK_HELP, N_("Contents"), NULL, N_("View the User's Manual."), (GCallback) gtk_app::mainmenu_HelpHelp },		//<control>H
	{ "About", GTK_STOCK_ABOUT, N_("About"), NULL, N_("Information about this program."), (GCallback) gtk_app::mainmenu_HelpAbout },	//<control>A
	
	// the rest are toolbar actions...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	{ "Element", "MY_ELEMENT_ICON", tb_shortcut_labels[0], NULL, N_("Set the current element."), (GCallback) gtk_app::maintb_dial_Element },
	{ "BondType", "MY_BONDTYPE_ICON", tb_shortcut_labels[1], NULL, N_("Set the current bondtype."), (GCallback) gtk_app::maintb_dial_BondType },
	{ "Setup", "MY_SETUP_ICON", tb_shortcut_labels[2], NULL, N_("Setup or change the comp.chem. method in use."), (GCallback) gtk_app::maintb_dial_Setup }
};

GtkToggleActionEntry gtk_app::entries2[] =
{
	{ "Draw", "MY_DRAW_ICON", tb_mousetool_labels[0], NULL, N_("Draw ; add atoms and bonds to the model."), (GCallback) gtk_app::maintb_tool_Draw, FALSE },
	{ "Erase", "MY_ERASE_ICON", tb_mousetool_labels[1], NULL, N_("Erase ; remove atoms and bonds from the model."), (GCallback) gtk_app::maintb_tool_Erase, FALSE },
	{ "Select", "MY_SELECT_ICON", tb_mousetool_labels[2], NULL, N_("Select ; make selections in the model, and also select objects."), (GCallback) gtk_app::maintb_tool_Select, FALSE },
	{ "Zoom", "MY_ZOOM_ICON", tb_mousetool_labels[3], NULL, N_("Zoom ; zoom the view."), (GCallback) gtk_app::maintb_tool_Zoom, FALSE },
	{ "Clip", "MY_CLIPPING_ICON", tb_mousetool_labels[4], NULL, N_("Clipping ; set the near and far clipping planes for graphics rendering."), (GCallback) gtk_app::maintb_tool_Clipping, FALSE },
	{ "TransXY", "MY_TRANSL_XY_ICON", tb_mousetool_labels[5], NULL, N_("TranslateXY ; translate the camera or a selected object in XY-direction."), (GCallback) gtk_app::maintb_tool_TranslateXY, FALSE },
	{ "TransZ", "MY_TRANSL_Z_ICON", tb_mousetool_labels[6], NULL, N_("TranslateZ ; translate the camera or a selected object in Z-direction."), (GCallback) gtk_app::maintb_tool_TranslateZ, FALSE },
	{ "OrbXY", "MY_ORBIT_XY_ICON", tb_mousetool_labels[7], NULL, N_("OrbitXY ; orbit the camera or a selected object around the focus point in XY-direction."), (GCallback) gtk_app::maintb_tool_OrbitXY, TRUE },
	{ "OrbZ", "MY_ORBIT_Z_ICON", tb_mousetool_labels[8], NULL, N_("OrbitZ ; orbit the camera or a selected object around the focus point in Z-direction."), (GCallback) gtk_app::maintb_tool_OrbitZ, FALSE },
	{ "RotXY", "MY_ROTATE_XY_ICON", tb_mousetool_labels[9], NULL, N_("RotateXY ; turn the camera or a selected object in XY-direction."), (GCallback) gtk_app::maintb_tool_RotateXY, FALSE },
	{ "RotZ", "MY_ROTATE_Z_ICON", tb_mousetool_labels[10], NULL, N_("RotateZ ; turn the camera or a selected object in Z-direction."), (GCallback) gtk_app::maintb_tool_RotateZ, FALSE },
	{ "Measure", "MY_MEASURE_ICON", tb_mousetool_labels[11], NULL, N_("Measure ; measure distances, angles and torsions."), (GCallback) gtk_app::maintb_tool_Measure, FALSE },
};

const char * gtk_app::ui_description =
"<ui>"
"  <menubar name='MainMenu'>"
"    <menu action='FileMenu'>"
"      <menuitem action='New'/>"
"      <menuitem action='Open'/>"
"      <separator/>"
"      <menuitem action='SaveAs'/>"
"      <separator/>"
"      <menuitem action='Close'/>"
"    </menu>"
"    <menu action='HelpMenu'>"
"      <menuitem action='Help'/>"
"      <menuitem action='About'/>"
"    </menu>"
"  </menubar>"
"  <toolbar name='MainTB'>"
"    <placeholder name='MainTools'>"
"      <separator/>"
"      <toolitem name='dr' action='Draw'/>"
"      <toolitem name='er' action='Erase'/>"
"      <toolitem name='se' action='Select'/>"
"      <toolitem name='zm' action='Zoom'/>"
"      <toolitem name='cp' action='Clip'/>"
"      <toolitem name='tt' action='TransXY'/>"
"      <toolitem name='tz' action='TransZ'/>"
"      <toolitem name='oo' action='OrbXY'/>"
"      <toolitem name='oz' action='OrbZ'/>"
"      <toolitem name='rr' action='RotXY'/>"
"      <toolitem name='rz' action='RotZ'/>"
"      <toolitem name='ms' action='Measure'/>"
"      <separator/>"
"      <toolitem name='el' action='Element'/>"
"      <toolitem name='bt' action='BondType'/>"
"      <separator/>"
"      <toolitem name='su' action='Setup'/>"
"      <separator/>"
"    </placeholder>"
"  </toolbar>"
"</ui>";

GtkUIManager * gtk_app::ui_manager = NULL;

GtkWidget * gtk_app::main_window = NULL;
GtkWidget * gtk_app::main_vbox = NULL;

GtkWidget * gtk_app::main_menubar = NULL;
GtkWidget * gtk_app::main_toolbar = NULL;

GtkWidget * gtk_app::mtb_mtool_draw = NULL;
GtkWidget * gtk_app::mtb_mtool_erase = NULL;
GtkWidget * gtk_app::mtb_mtool_select = NULL;
GtkWidget * gtk_app::mtb_mtool_zoom = NULL;
GtkWidget * gtk_app::mtb_mtool_clipping = NULL;
GtkWidget * gtk_app::mtb_mtool_translate_xy = NULL;
GtkWidget * gtk_app::mtb_mtool_translate_z = NULL;
GtkWidget * gtk_app::mtb_mtool_orbit_xy = NULL;
GtkWidget * gtk_app::mtb_mtool_orbit_z = NULL;
GtkWidget * gtk_app::mtb_mtool_rotate_xy = NULL;
GtkWidget * gtk_app::mtb_mtool_rotate_z = NULL;
GtkWidget * gtk_app::mtb_mtool_measure = NULL;

GtkWidget * gtk_app::paned_widget = NULL;

GtkWidget * gtk_app::notebook_widget = NULL;

GtkTextBuffer * gtk_app::txt_buffer = NULL;
GtkTextMark * gtk_app::end_mark = NULL;

GtkWidget * gtk_app::scroll_widget = NULL;
GtkWidget * gtk_app::txt_widget = NULL;

GtkWidget * gtk_app::pv_view_widget = NULL;
GtkWidget * gtk_app::pv_label_widget = NULL;

// the views/objects-menu...
// ^^^^^^^^^^^^^^^^^^^^^^^^^

GtkActionEntry gtk_app::pv_viewsobjs_entries[] =
{
	{ "viewsobjs_SetCurrent", NULL, N_("Set to Current Object"), NULL, N_("Set this object to Current Object"), (GCallback) gtk_app::viewsobjs_SetCurrent },
	{ "viewsobjs_Delete", NULL, N_("Delete Object/View"), NULL, N_("Delete this object or view"), (GCallback) gtk_app::viewsobjs_Delete },
};

const char * gtk_app::pv_viewsobjs_ui_description =
"<ui>"
"  <popup name='gpvViewsObjsMenu'>"
"    <menuitem action='viewsobjs_SetCurrent'/>"
"    <separator/>"
"    <menuitem action='viewsobjs_Delete'/>"
"  </popup>"
"</ui>";

// the chains-menu...
// ^^^^^^^^^^^^^^^^^^

GtkActionEntry gtk_app::pv_chains_entries[] =
{
	{ "chains_UpdateView", NULL, N_("Update View"), NULL, N_("Rebuild the chains info for this view"), (GCallback) gtk_app::chains_UpdateView },
	{ "chains_SelectItem", NULL, N_("Select Item"), NULL, N_("Select/unselect this chain/residue"), (GCallback) gtk_app::chains_SelectItem },
};

const char * gtk_app::pv_chains_ui_description =
"<ui>"
"  <popup name='gpvChainsMenu'>"
"    <menuitem action='chains_UpdateView'/>"
"    <separator/>"
"    <menuitem action='chains_SelectItem'/>"
"  </popup>"
"</ui>";

// the atoms-menu...
// ^^^^^^^^^^^^^^^^^

GtkActionEntry gtk_app::pv_atoms_entries[] =
{
	{ "atoms_SelectAtom", NULL, N_("Select Atom"), NULL, N_("Select/unselect this atom"), (GCallback) gtk_app::atoms_SelectAtom },
};

const char * gtk_app::pv_atoms_ui_description =
"<ui>"
"  <popup name='gpvAtomsMenu'>"
"    <menuitem action='atoms_SelectAtom'/>"
"  </popup>"
"</ui>";

// the bonds-menu...
// ^^^^^^^^^^^^^^^^^

GtkActionEntry gtk_app::pv_bonds_entries[] =
{
	{ "bonds_SelectBond", NULL, N_("Select Bond"), NULL, N_("Select/unselect this bond"), (GCallback) gtk_app::bonds_SelectBond },
};

const char * gtk_app::pv_bonds_ui_description =
"<ui>"
"  <popup name='gpvBondsMenu'>"
"    <menuitem action='bonds_SelectBond'/>"
"  </popup>"
"</ui>";

// end of pv-pages.
// ^^^^^^^^^^^^^^^^

gtk_app::gtk_app(void) :
	custom_app()
{
	// register some new stock icons...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	GtkIconFactory * icon_factory = gtk_icon_factory_new();
	GtkIconSet * icon_set; GdkPixbuf * pixbuf; GtkIconSource * icon_source;
	const int num_icons = 12 + 3;
	
	const char ** icondata[num_icons] =
	{
		(const char **) draw_xpm,
		(const char **) erase_xpm,
		(const char **) select_xpm,
		(const char **) zoom_xpm,
		(const char **) clipping_xpm,
		(const char **) transl_xy_xpm,
		(const char **) transl_z_xpm,
		(const char **) orbit_xy_xpm,
		(const char **) orbit_z_xpm,
		(const char **) rotate_xy_xpm,
		(const char **) rotate_z_xpm,
		(const char **) measure_xpm,
		
		(const char **) element_xpm,
		(const char **) bondtype_xpm,
		(const char **) setup_xpm
	};
	
	const char * icon_id[num_icons] =
	{
		"MY_DRAW_ICON",
		"MY_ERASE_ICON",
		"MY_SELECT_ICON",
		"MY_ZOOM_ICON",
		"MY_CLIPPING_ICON",
		"MY_TRANSL_XY_ICON",
		"MY_TRANSL_Z_ICON",
		"MY_ORBIT_XY_ICON",
		"MY_ORBIT_Z_ICON",
		"MY_ROTATE_XY_ICON",
		"MY_ROTATE_Z_ICON",
		"MY_MEASURE_ICON",
		
		"MY_ELEMENT_ICON",
		"MY_BONDTYPE_ICON",
		"MY_SETUP_ICON"
	};
	
	for (int ii = 0;ii < num_icons;ii++)
	{
		pixbuf = gdk_pixbuf_new_from_xpm_data(icondata[ii]);
		icon_set = gtk_icon_set_new_from_pixbuf(pixbuf);
		
		icon_source = gtk_icon_source_new();
		gtk_icon_source_set_pixbuf(icon_source, pixbuf);
		gtk_icon_set_add_source(icon_set, icon_source);
		gtk_icon_source_free (icon_source);
		
		gtk_icon_factory_add(icon_factory, icon_id[ii], icon_set);
		gtk_icon_set_unref (icon_set);
	}
	
	gtk_icon_factory_add_default(icon_factory);
	
	// create the main window...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^
	
	main_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	
	gtk_window_set_default_size(GTK_WINDOW(main_window), 845, 640);
	
gchar * tmp_title = g_strconcat(_("Ghemical"), " ", APPVERSION, NULL );
gtk_window_set_title(GTK_WINDOW(main_window), tmp_title);
free(tmp_title); tmp_title = NULL;
	
	ostringstream icon_fn;
	icon_fn << project::appdata_path << DIR_SEPARATOR << APPVERSION << DIR_SEPARATOR << "pixmaps" << DIR_SEPARATOR << "ghemical.png" << ends;
	pixbuf = gdk_pixbuf_new_from_file(icon_fn.str().c_str(), NULL);
	if (pixbuf == NULL) printf(_("ERROR : Icon loading failed : %s\n"), icon_fn.str().c_str());
	else gtk_window_set_icon(GTK_WINDOW(main_window), pixbuf);
	
	gtk_container_set_border_width(GTK_CONTAINER(main_window), 1);
	
	g_signal_connect(G_OBJECT(main_window), "delete_event", G_CALLBACK(gtk_app::DeleteEventHandler), NULL);
	g_signal_connect(G_OBJECT(main_window), "destroy", G_CALLBACK(gtk_app::DestroyHandler), NULL);
	
	main_vbox = gtk_vbox_new(FALSE, 0);
	gtk_container_set_border_width(GTK_CONTAINER(main_vbox), 1);
	gtk_container_add(GTK_CONTAINER(main_window), main_vbox);
	
	// add the user interface elements...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	GtkActionGroup * action_group1 = gtk_action_group_new("MainWindowActions");
	gtk_action_group_set_translation_domain(action_group1, GETTEXT_PACKAGE);
	gtk_action_group_add_actions(action_group1, entries1, G_N_ELEMENTS(entries1), NULL);
	
	GtkActionGroup * action_group2 = gtk_action_group_new("MouseToolToggleActions");
	gtk_action_group_set_translation_domain(action_group2, GETTEXT_PACKAGE);
	gtk_action_group_add_toggle_actions(action_group2, entries2, G_N_ELEMENTS(entries2), NULL);
	
	ui_manager = gtk_ui_manager_new();
	gtk_ui_manager_insert_action_group(ui_manager, action_group1, 0);
	gtk_ui_manager_insert_action_group(ui_manager, action_group2, 0);
	
	GError * error = NULL;
	if (!gtk_ui_manager_add_ui_from_string(ui_manager, ui_description, -1, & error))
	{
		g_message(_("ERROR : Building main menu failed : %s"), error->message);
		g_error_free(error); exit(EXIT_FAILURE);
	}
	
	// todo : tooltips do not appear to the main menu?!?!?
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	// but they DO appear in the toolbar??? TOOLTIPS NEEDED FOR I18N!!!
	// gedit shows tooltips in the statusbar. at GtkUIManager documentation
	// "connect-proxy" -signal is defined which may have something to do with this.
	
	main_menubar = gtk_ui_manager_get_widget(ui_manager, "/MainMenu");
	gtk_box_pack_start(GTK_BOX(main_vbox), main_menubar, FALSE, FALSE, 0);
	
	main_toolbar = gtk_ui_manager_get_widget(ui_manager, "/MainTB");
	gtk_box_pack_start(GTK_BOX(main_vbox), main_toolbar, FALSE, FALSE, 0);
	
mtb_mtool_draw = gtk_ui_manager_get_widget(ui_manager, "/MainTB/MainTools/dr");
mtb_mtool_erase = gtk_ui_manager_get_widget(ui_manager, "/MainTB/MainTools/er");
mtb_mtool_select = gtk_ui_manager_get_widget(ui_manager, "/MainTB/MainTools/se");
mtb_mtool_zoom = gtk_ui_manager_get_widget(ui_manager, "/MainTB/MainTools/zm");
mtb_mtool_clipping = gtk_ui_manager_get_widget(ui_manager, "/MainTB/MainTools/cp");
mtb_mtool_translate_xy = gtk_ui_manager_get_widget(ui_manager, "/MainTB/MainTools/tt");
mtb_mtool_translate_z = gtk_ui_manager_get_widget(ui_manager, "/MainTB/MainTools/tz");
mtb_mtool_orbit_xy = gtk_ui_manager_get_widget(ui_manager, "/MainTB/MainTools/oo");
mtb_mtool_orbit_z = gtk_ui_manager_get_widget(ui_manager, "/MainTB/MainTools/oz");
mtb_mtool_rotate_xy = gtk_ui_manager_get_widget(ui_manager, "/MainTB/MainTools/rr");
mtb_mtool_rotate_z = gtk_ui_manager_get_widget(ui_manager, "/MainTB/MainTools/rz");
mtb_mtool_measure = gtk_ui_manager_get_widget(ui_manager, "/MainTB/MainTools/ms");
	
	paned_widget = gtk_vpaned_new();
	
	notebook_widget = gtk_notebook_new();
	
	txt_widget = gtk_text_view_new();
	gtk_text_view_set_editable(GTK_TEXT_VIEW(txt_widget), false);
	
	txt_buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(txt_widget));
	GtkTextIter txt_iter; gtk_text_buffer_get_end_iter(txt_buffer, & txt_iter);
	end_mark = gtk_text_buffer_create_mark(txt_buffer, NULL, & txt_iter, FALSE);	// right_gravity!
	
	scroll_widget = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_add(GTK_CONTAINER(scroll_widget), GTK_WIDGET(txt_widget));
	
	gtk_widget_show(txt_widget);
	
	gtk_paned_add1(GTK_PANED(paned_widget), GTK_WIDGET(notebook_widget));
	gtk_paned_add2(GTK_PANED(paned_widget), GTK_WIDGET(scroll_widget));
	
	gtk_box_pack_start(GTK_BOX(main_vbox), paned_widget, TRUE, TRUE, 0);
	
	gtk_widget_show(notebook_widget);
	gtk_widget_show(scroll_widget);
	
	gtk_widget_show(paned_widget);
	
	InitPV();	// build and show the project_view...
	
	// show the widgets and enter in the main loop.
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	gtk_widget_show(main_menubar);
	gtk_widget_show(main_toolbar);
	
	gtk_widget_show(main_vbox);
	gtk_widget_show(main_window);
	
	// set a new default project.
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	SetNewProject();
	
	// when leaving this stage, the program
	// will (soon) enter in the main loop...
}

gtk_app::~gtk_app(void)
{
	// need to release memory etc...
}

gtk_app * gtk_app::GetAppX(void)
{
	base_app * app = base_app::GetAppB();
	if (app != NULL) return dynamic_cast<gtk_app *>(app);
	else return new gtk_app();
}

gtk_project * gtk_app::GetPrjX(void)
{
	project * p = custom_app::GetPrj();
	if (!p) return NULL;
	
	gtk_project * gp = dynamic_cast<gtk_project *>(p);
	return gp;
}

void gtk_app::AttachDetachView(base_wcl * wcl)
{
	// this is a gtk-notebook-related special feature,
	// so it will duplicate some functionality from the
	// generic implementations (project-class???).
	
	// so also see these:
	// ^^^^^^^^^^^^^^^^^^
	// project::AddGraphicsClient()
	// project::RemoveGraphicsClient()
	
	base_wnd * wndB = wcl->GetWnd();
	gtk_wnd * wndX = dynamic_cast<gtk_wnd *>(wndB);
	
	if (wndB == NULL || wndX == NULL)
	{
		assertion_failed(__FILE__, __LINE__, "no wnd!");
	}
	
	bool detached = !wndX->IsDetached();
	wndX = NULL;	// no longer needed...
	
	// unlink the old window, and destroy it.
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	wcl->UnlinkWnd();
	
	// then create a new window, and link it.
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	// determine the window class using the client...
	
	if (dynamic_cast<oglview_wcl *>(wcl) != NULL)
	{
		GetPrj()->DestroyGraphicsWnd(wndB);
		wndB = NULL;
		
		wndB = GetPrj()->CreateGraphicsWnd(detached);
	}
	else
	{
		GetPrj()->DestroyPlottingWnd(wndB);
		wndB = NULL;
		
		// handle the various plotting wnd types here...
		
		if (dynamic_cast<p1dview_wcl *>(wcl) != NULL)	wndB = GetPrj()->CreatePlot1DWnd(detached);
	else	if (dynamic_cast<p2dview_wcl *>(wcl) != NULL)	wndB = GetPrj()->CreatePlot2DWnd(detached);
	else	if (dynamic_cast<eldview_wcl *>(wcl) != NULL)	wndB = GetPrj()->CreateEnergyLevelDiagramWnd(detached);
	else	if (dynamic_cast<rcpview_wcl *>(wcl) != NULL)	wndB = GetPrj()->CreateReactionCoordinatePlotWnd(detached);
	else	if (dynamic_cast<gpcview_wcl *>(wcl) != NULL)	wndB = GetPrj()->CreateGenericProteinChainWnd(detached);
		else
		{
			assertion_failed(__FILE__, __LINE__, "wcl class unknown.");
		}
	}
	
	wcl->LinkWnd(wndB);
	
	UpdateAllWindowTitles();	// attach/detach status texts...
}

void gtk_app::SetTransientForMainWnd(GtkWindow * other_window)
{
	gtk_window_set_transient_for(other_window, GTK_WINDOW(main_window));
}

void gtk_app::AddTabToNB(GtkWidget * widget, GtkWidget * label)
{
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook_widget), widget, label);
	
	// activate the last page ; makes the new view visible.
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	gtk_notebook_set_page(GTK_NOTEBOOK(notebook_widget), -1);
}

void gtk_app::RemoveTabFromNB(GtkWidget * widget)
{
	gint page = gtk_notebook_page_num(GTK_NOTEBOOK(notebook_widget), widget);
	gtk_notebook_remove_page(GTK_NOTEBOOK(notebook_widget), page);
}

void gtk_app::SetTabTitleNB(GtkWidget * widget, GtkWidget * label)
{
	gtk_notebook_set_tab_label(GTK_NOTEBOOK(notebook_widget), widget, label);
}

void gtk_app::InitPV(void)
{
	pv_label_widget = gtk_label_new(_("project view"));
	
	pv_view_widget = gtk_notebook_new();
	gtk_widget_set_size_request(pv_view_widget, 640, 400);	// minimum size...
	
	GtkCellRenderer * renderer;
	GtkTreeViewColumn * column;
	
	GtkActionGroup * action_group = NULL;
	GError * error = NULL;
	
	// "views/objects"-page
	
	pv_viewsobjs_store = gtk_tree_store_new (2, G_TYPE_STRING, G_TYPE_STRING);
	pv_viewsobjs_widget = gtk_tree_view_new_with_model(GTK_TREE_MODEL(pv_viewsobjs_store));
	gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(pv_viewsobjs_widget), TRUE);		// optional : draw the stripes to background.
	
	renderer = gtk_cell_renderer_text_new();	// ??? (string)
	column = gtk_tree_view_column_new_with_attributes(_("Object"), renderer, "text", 0, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(pv_viewsobjs_widget), column);
	
	renderer = gtk_cell_renderer_text_new();	// ??? (string)
	column = gtk_tree_view_column_new_with_attributes(_("Sub-Objects"), renderer, "text", 1, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(pv_viewsobjs_widget), column);
	
	pv_viewsobjs_label = gtk_label_new(_("Views/Objects"));
	pv_viewsobjs_sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(pv_viewsobjs_sw), GTK_SHADOW_ETCHED_IN);	// optional : ???
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(pv_viewsobjs_sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add(GTK_CONTAINER(pv_viewsobjs_sw), pv_viewsobjs_widget);
	gtk_notebook_append_page(GTK_NOTEBOOK(pv_view_widget), pv_viewsobjs_sw, pv_viewsobjs_label);
action_group = gtk_action_group_new("gpvViewsObjsActions");
gtk_action_group_set_translation_domain(action_group, GETTEXT_PACKAGE);
gtk_action_group_add_actions(action_group, pv_viewsobjs_entries, G_N_ELEMENTS(pv_viewsobjs_entries), GTK_WIDGET(pv_view_widget));
gtk_ui_manager_insert_action_group(gtk_app::GetUIManager(), action_group, 0);
error = NULL;
if (!gtk_ui_manager_add_ui_from_string(gtk_app::GetUIManager(), pv_viewsobjs_ui_description, -1, & error))
{
	g_message(_("ERROR : Building Views/Objects menu in project view failed : %s"), error->message);
	g_error_free(error); exit(EXIT_FAILURE);
}
pv_viewsobjs_menu = gtk_ui_manager_get_widget(gtk_app::GetUIManager(), "/gpvViewsObjsMenu");
g_signal_connect_swapped(GTK_OBJECT(pv_viewsobjs_widget), "button_press_event", G_CALLBACK(ViewsObjsPopupHandler), GTK_WIDGET(pv_viewsobjs_widget));
	gtk_widget_show(pv_viewsobjs_widget);
	gtk_widget_show(pv_viewsobjs_label);
	gtk_widget_show(pv_viewsobjs_sw);
	
	// "chains"-page
	
	pv_chains_store = gtk_tree_store_new (5, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
	pv_chains_widget = gtk_tree_view_new_with_model(GTK_TREE_MODEL(pv_chains_store));
	gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(pv_chains_widget), TRUE);		// optional : draw the stripes to background.
	
	renderer = gtk_cell_renderer_text_new();	// chain_info (string)
	column = gtk_tree_view_column_new_with_attributes(_("Chain Description"), renderer, "text", 0, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(pv_chains_widget), column);
	
	renderer = gtk_cell_renderer_text_new();	// res_num (string)
	column = gtk_tree_view_column_new_with_attributes(_("Residue Number"), renderer, "text", 1, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(pv_chains_widget), column);
	
	renderer = gtk_cell_renderer_text_new();	// res_id (string)
	column = gtk_tree_view_column_new_with_attributes(_("Residue ID"), renderer, "text", 2, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(pv_chains_widget), column);
	
	renderer = gtk_cell_renderer_text_new();	// res_state1 (string)
	column = gtk_tree_view_column_new_with_attributes(_("Sec-Str State"), renderer, "text", 3, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(pv_chains_widget), column);
	
	renderer = gtk_cell_renderer_text_new();	// res_state2 (string)
	column = gtk_tree_view_column_new_with_attributes(_("Protonation State"), renderer, "text", 4, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(pv_chains_widget), column);
	
	pv_chains_label = gtk_label_new(_("Chains"));
	pv_chains_sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(pv_chains_sw), GTK_SHADOW_ETCHED_IN);		// optional : ???
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(pv_chains_sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add(GTK_CONTAINER(pv_chains_sw), pv_chains_widget);
	gtk_notebook_append_page(GTK_NOTEBOOK(pv_view_widget), pv_chains_sw, pv_chains_label);
action_group = gtk_action_group_new("gpvChainsActions");
gtk_action_group_set_translation_domain(action_group, GETTEXT_PACKAGE);
gtk_action_group_add_actions(action_group, pv_chains_entries, G_N_ELEMENTS(pv_chains_entries), GTK_WIDGET(pv_view_widget));
gtk_ui_manager_insert_action_group(gtk_app::GetUIManager(), action_group, 0);
error = NULL;
if (!gtk_ui_manager_add_ui_from_string(gtk_app::GetUIManager(), pv_chains_ui_description, -1, & error))
{
	g_message(_("ERROR : Building Chains menu in project view failed : %s"), error->message);
	g_error_free(error); exit(EXIT_FAILURE);
}
pv_chains_menu = gtk_ui_manager_get_widget(gtk_app::GetUIManager(), "/gpvChainsMenu");
g_signal_connect_swapped(GTK_OBJECT(pv_chains_widget), "button_press_event", G_CALLBACK(ChainsPopupHandler), GTK_WIDGET(pv_chains_widget));
	gtk_widget_show(pv_chains_widget);
	gtk_widget_show(pv_chains_label);
	gtk_widget_show(pv_chains_sw);
	
	// "atoms"-page
	
	pv_atoms_store = gtk_list_store_new (3, G_TYPE_UINT, G_TYPE_STRING, G_TYPE_BOOLEAN);
	pv_atoms_widget = gtk_tree_view_new_with_model(GTK_TREE_MODEL(pv_atoms_store));
	gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(pv_atoms_widget), TRUE);		// optional : draw the stripes to background.
	
	renderer = gtk_cell_renderer_text_new();	// index1
	column = gtk_tree_view_column_new_with_attributes(_("Atom Index"), renderer, "text", 0, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(pv_atoms_widget), column);
	
	renderer = gtk_cell_renderer_text_new();	// element
	column = gtk_tree_view_column_new_with_attributes(_("Element"), renderer, "text", 1, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(pv_atoms_widget), column);
	
	renderer = gtk_cell_renderer_toggle_new();	// locked
	g_object_set(renderer, "activatable", TRUE, NULL);
	g_signal_connect(GTK_OBJECT(renderer), "toggled", G_CALLBACK(atoms_ToggleLocked), (gpointer *) pv_atoms_store);
	column = gtk_tree_view_column_new_with_attributes(_("Locked"), renderer, "active", 2, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(pv_atoms_widget), column);
	
	pv_atoms_label = gtk_label_new(_("Atoms"));
	pv_atoms_sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(pv_atoms_sw), GTK_SHADOW_ETCHED_IN);		// optional : ???
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(pv_atoms_sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add(GTK_CONTAINER(pv_atoms_sw), pv_atoms_widget);
	gtk_notebook_append_page(GTK_NOTEBOOK(pv_view_widget), pv_atoms_sw, pv_atoms_label);
action_group = gtk_action_group_new("gpvAtomsActions");
gtk_action_group_set_translation_domain(action_group, GETTEXT_PACKAGE);
gtk_action_group_add_actions(action_group, pv_atoms_entries, G_N_ELEMENTS(pv_atoms_entries), GTK_WIDGET(pv_view_widget));
gtk_ui_manager_insert_action_group(gtk_app::GetUIManager(), action_group, 0);
error = NULL;
if (!gtk_ui_manager_add_ui_from_string(gtk_app::GetUIManager(), pv_atoms_ui_description, -1, & error))
{
	g_message(_("ERROR : Building Atoms menu in project view failed : %s"), error->message);
	g_error_free(error); exit(EXIT_FAILURE);
}
pv_atoms_menu = gtk_ui_manager_get_widget(gtk_app::GetUIManager(), "/gpvAtomsMenu");
g_signal_connect_swapped(GTK_OBJECT(pv_atoms_widget), "button_press_event", G_CALLBACK(AtomsPopupHandler), GTK_WIDGET(pv_atoms_widget));
	gtk_widget_show(pv_atoms_widget);
	gtk_widget_show(pv_atoms_label);
	gtk_widget_show(pv_atoms_sw);
	
	// "bonds"-page
	
	pv_bonds_store = gtk_list_store_new (3, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_STRING);
	pv_bonds_widget = gtk_tree_view_new_with_model(GTK_TREE_MODEL(pv_bonds_store));
	gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(pv_bonds_widget), TRUE);		// optional : draw the stripes to background.
	
	renderer = gtk_cell_renderer_text_new();	// index1
	column = gtk_tree_view_column_new_with_attributes(_("Atom Index #1"), renderer, "text", 0, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(pv_bonds_widget), column);
	
	renderer = gtk_cell_renderer_text_new();	// index2
	column = gtk_tree_view_column_new_with_attributes(_("Atom Index #2"), renderer, "text", 1, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(pv_bonds_widget), column);
	
	renderer = gtk_cell_renderer_text_new();	// bondtype
	column = gtk_tree_view_column_new_with_attributes(_("BondType"), renderer, "text", 2, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(pv_bonds_widget), column);
	
	pv_bonds_label = gtk_label_new(_("Bonds"));
	pv_bonds_sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(pv_bonds_sw), GTK_SHADOW_ETCHED_IN);		// optional : ???
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(pv_bonds_sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add(GTK_CONTAINER(pv_bonds_sw), pv_bonds_widget);
	gtk_notebook_append_page(GTK_NOTEBOOK(pv_view_widget), pv_bonds_sw, pv_bonds_label);
action_group = gtk_action_group_new("gpvBondsActions");
gtk_action_group_set_translation_domain(action_group, GETTEXT_PACKAGE);
gtk_action_group_add_actions(action_group, pv_bonds_entries, G_N_ELEMENTS(pv_bonds_entries), GTK_WIDGET(pv_view_widget));
gtk_ui_manager_insert_action_group(gtk_app::GetUIManager(), action_group, 0);
error = NULL;
if (!gtk_ui_manager_add_ui_from_string(gtk_app::GetUIManager(), pv_bonds_ui_description, -1, & error))
{
	g_message(_("ERROR : Building Bonds menu in project view failed : %s"), error->message);
	g_error_free(error); exit(EXIT_FAILURE);
}
pv_bonds_menu = gtk_ui_manager_get_widget(gtk_app::GetUIManager(), "/gpvBondsMenu");
g_signal_connect_swapped(GTK_OBJECT(pv_bonds_widget), "button_press_event", G_CALLBACK(BondsPopupHandler), GTK_WIDGET(pv_bonds_widget));
	gtk_widget_show(pv_bonds_widget);
	gtk_widget_show(pv_bonds_label);
	gtk_widget_show(pv_bonds_sw);
	
	// ready...
	// ready...
	// ready...
	
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook_widget), pv_view_widget, pv_label_widget);
	
	gtk_widget_show(GTK_WIDGET(pv_view_widget));
	gtk_widget_show(GTK_WIDGET(pv_label_widget));
}

void gtk_app::SetNewProject(void)
{
	if (prj != NULL)
	{
		prj->ClearAll();
		delete prj; prj = NULL;
	}
	
	custom_app::SetNewProject();
	gtk_project * tmpprj = new gtk_project();
	
	prj = tmpprj;
	tmpprj->DoSafeStart();
}

gboolean gtk_app::DeleteEventHandler(GtkWidget * widget, GdkEvent * event, gpointer data)
{
	if (project::background_job_running) return TRUE;	// protect the model-data during background jobs...
	
	bool quit = sQuestion(_("Are you sure that you\nwant to quit the program?"));
	if (quit) return FALSE; else return TRUE;
}

void gtk_app::DestroyHandler(GtkWidget * widget, gpointer data)
{
	prj->ClearAll();
	delete prj; prj = NULL;
	
	gtk_main_quit();
}

// Print the message (no problems).
void gtk_app::sMessage(const char * msg)
{
	GtkWidget * message_dialog = gtk_message_dialog_new(NULL,
	GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, GTK_BUTTONS_OK, "%s", msg);
	
	gtk_dialog_run(GTK_DIALOG(message_dialog));
	gtk_widget_destroy(message_dialog);
}

// Print the message (lower severity).
void gtk_app::sWarningMessage(const char * msg)
{
	GtkWidget * message_dialog = gtk_message_dialog_new(NULL,
	GTK_DIALOG_MODAL, GTK_MESSAGE_WARNING, GTK_BUTTONS_OK, "%s", msg);
	
	gtk_dialog_run(GTK_DIALOG(message_dialog));
	gtk_widget_destroy(message_dialog);
}

// Print the message (higher severity).
void gtk_app::sErrorMessage(const char * msg)
{
	GtkWidget * message_dialog = gtk_message_dialog_new(NULL,
	GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, "%s", msg);
	
	gtk_dialog_run(GTK_DIALOG(message_dialog));
	gtk_widget_destroy(message_dialog);
}

// Print the message and wait for a yes/no response.
bool gtk_app::sQuestion(const char * msg)
{
	GtkWidget * question_dialog = gtk_message_dialog_new(NULL,
	GTK_DIALOG_MODAL, GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, "%s", msg);
	
	gint response = gtk_dialog_run(GTK_DIALOG(question_dialog));
	gtk_widget_destroy(question_dialog);
	
	switch (response)
	{
		case GTK_RESPONSE_YES:
		return true;
		
		default:
		return false;
	}
}

#define MSG_BUFF_SZ	65536

void gtk_app::sPrintToLog(const char * msg)
{
	if (strlen(msg) > MSG_BUFF_SZ)
	{
		cout << _("gtk_app::sPrintToLog() : message is too long!") << endl;
		return;
	}
	
	static char msgbuff[MSG_BUFF_SZ];
	strcpy(msgbuff, msg);
	
	GtkTextIter txt_iter;
	gtk_text_buffer_get_iter_at_mark(txt_buffer, & txt_iter, end_mark);
	
	gtk_text_buffer_insert(txt_buffer, & txt_iter, msgbuff, -1);
	
	gtk_text_view_scroll_mark_onscreen(GTK_TEXT_VIEW(txt_widget), end_mark);
	
	cout << "PrintToLog : " << msg;
}

void gtk_app::Message(const char * msg)
{
	sMessage(msg);
}

void gtk_app::WarningMessage(const char * msg)
{
	sWarningMessage(msg);
}

void gtk_app::ErrorMessage(const char * msg)
{
	sErrorMessage(msg);
}

bool gtk_app::Question(const char * msg)
{
	return sQuestion(msg);
}

void gtk_app::PrintToLog(const char * msg)
{
	sPrintToLog(msg);
}

void gtk_app::UpdateAllWindowTitles(void)
{
	list<pv_viewsobjs_record *>::iterator it = pv_viewsobjs_data.begin();
	while (it != pv_viewsobjs_data.end())
	{
		if ((* it)->refV2 != NULL)
		{
			base_wcl * wcl = (* it)->refV2;
			
			ostringstream vts;	// view title stream (in PV)
			ostringstream wts;	// window title stream
			
			oglview_wcl * oglwcl = dynamic_cast<oglview_wcl *>(wcl);
			if (oglwcl != NULL)
			{
				gtk_wnd * wnd = dynamic_cast<gtk_wnd *>(oglwcl->GetWnd());
				
				vts << _("window ") << oglwcl->my_wnd_number;
				vts << (wnd->IsDetached() ? _(" (detached)") : _(" (attached)")) << ends;
				
				wts << _("cam ") << oglwcl->GetCCam()->GetCCamI() << " ";
				wts << _("wnd ") << oglwcl->my_wnd_number;
				wts << (wnd->IsDetached() ? _(" (detached)") : "") << ends;
				
				oglwcl->SetTitle(wts.str().c_str());
			}
			else
			{
				// it's a plotting client...
				// ^^^^^^^^^^^^^^^^^^^^^^^^^
				
				bool has_name = false;
				
				if (dynamic_cast<ac_stor_wcl *>(wcl) != NULL)
				{
					if (dynamic_cast<p1dview_wcl *>(wcl) != NULL)
					{
						if (dynamic_cast<rcpview_wcl *>(wcl) != NULL)
						{
							has_name = true;
							vts << _("RC plot view") << ends;
							wts << _("RC plot view") << ends;
						}
						else
						{
							has_name = true;
							vts << _("1D plot view") << ends;
							wts << _("1D plot view") << ends;
						}
					}
					else if (dynamic_cast<p2dview_wcl *>(wcl) != NULL)
					{
						has_name = true;
						vts << _("2D plot view") << ends;
						wts << _("2D plot view") << ends;
					}
				}
				else if (dynamic_cast<eldview_wcl *>(wcl) != NULL)
				{
					has_name = true;
					vts << _("energy-level diagram view") << ends;
					wts << _("energy-level diagram view") << ends;
				}
				else if (dynamic_cast<gpcview_wcl *>(wcl) != NULL)
				{
					has_name = true;
					vts << _("generic protein chain view") << ends;
					wts << _("generic protein chain view") << ends;
				}
				
				if (!has_name)
				{
					vts << _("<unknown view>") << ends;
					wts << _("<unknown view>") << ends;
				}
				
				wcl->SetTitle(wts.str().c_str());
			}
			
			gtk_tree_store_set(pv_viewsobjs_store, & (* it)->iter, 0, vts.str().c_str(), 1, FALSE, -1);
		}
		
		it++;
	}
}

void gtk_app::CameraAdded(custom_camera * p1)
{
	pv_viewsobjs_record * vo_rec = new pv_viewsobjs_record;
	
	vo_rec->owner = NULL;
	vo_rec->refV1 = p1;
	vo_rec->refV2 = NULL;
	vo_rec->refO = NULL;
	
	gtk_tree_store_append(pv_viewsobjs_store, & vo_rec->iter, NULL);
	pv_viewsobjs_data.push_back(vo_rec);
	
	ostringstream name;
	name << _("camera ") << p1->GetCCamI() << ends;
	const char * name_str = name.str().c_str();
	
	gtk_tree_store_set(pv_viewsobjs_store, & vo_rec->iter, 0, name_str, 1, FALSE, -1);
}

void gtk_app::CameraRemoved(custom_camera * p1)
{
	list<pv_viewsobjs_record *>::iterator it = pv_viewsobjs_data.begin();
	while (it != pv_viewsobjs_data.end()) { if ((* it)->refV1 == (ogl_dummy_object *) p1) break; else it++; }
	
	if (it == pv_viewsobjs_data.end())
	{
		assertion_failed(__FILE__, __LINE__, "object not found.");
	}
	
	gtk_tree_store_remove(pv_viewsobjs_store, & (* it)->iter);
	
	pv_viewsobjs_data.erase(it);
	delete (* it);
}

void gtk_app::LightAdded(ogl_light * p1)
{
	bool is_local_light = (p1->owner != NULL);
	if (is_local_light)
	{
		list<pv_viewsobjs_record *>::iterator it = pv_viewsobjs_data.begin();
		while (it != pv_viewsobjs_data.end()) { if ((* it)->refV1 == (ogl_dummy_object *) p1->owner) break; else it++; }
		
		if (it == pv_viewsobjs_data.end())
		{
			assertion_failed(__FILE__, __LINE__, "owner not found.");
		}
		
		custom_camera * ccam = dynamic_cast<custom_camera *>(p1->owner);
		if (!ccam)
		{
			assertion_failed(__FILE__, __LINE__, "ccam cast failed.");
		}
		
		pv_viewsobjs_record * vo_rec = new pv_viewsobjs_record;
		
		vo_rec->owner = ccam;
		vo_rec->refV1 = p1;
		vo_rec->refV2 = NULL;
		vo_rec->refO = NULL;
		
		gtk_tree_store_append(pv_viewsobjs_store, & vo_rec->iter, & (* it)->iter);
		pv_viewsobjs_data.push_back(vo_rec);
		
		const char * object_name = p1->GetObjectName();
		
		gtk_tree_store_set(pv_viewsobjs_store, & vo_rec->iter, 0, object_name, 1, FALSE, -1);
		
		// expand the newly created row...
		
		GtkTreePath * path = gtk_tree_model_get_path(GTK_TREE_MODEL(pv_viewsobjs_store), & vo_rec->iter);
		gtk_tree_view_expand_to_path(GTK_TREE_VIEW(pv_viewsobjs_widget), path);
		gtk_tree_path_free(path);
	}
	else
	{
		pv_viewsobjs_record * vo_rec = new pv_viewsobjs_record;
		
		vo_rec->owner = NULL;
		vo_rec->refV1 = p1;
		vo_rec->refV2 = NULL;
		vo_rec->refO = NULL;
		
		gtk_tree_store_append(pv_viewsobjs_store, & vo_rec->iter, NULL);
		pv_viewsobjs_data.push_back(vo_rec);
		
		const char * object_name = p1->GetObjectName();
		
		gtk_tree_store_set(pv_viewsobjs_store, & vo_rec->iter, 0, object_name, 1, FALSE, -1);
	}
}

void gtk_app::LightRemoved(ogl_light * p1)
{
	list<pv_viewsobjs_record *>::iterator it = pv_viewsobjs_data.begin();
	while (it != pv_viewsobjs_data.end()) { if ((* it)->refV1 == (ogl_dummy_object *) p1) break; else it++; }
	
	if (it == pv_viewsobjs_data.end())
	{
		assertion_failed(__FILE__, __LINE__, "object not found.");
	}
	
	gtk_tree_store_remove(pv_viewsobjs_store, & (* it)->iter);
	
	pv_viewsobjs_data.erase(it);
	delete (* it);
}

void gtk_app::GraphicsClientAdded(oglview_wcl * p1)
{
	list<pv_viewsobjs_record *>::iterator it = pv_viewsobjs_data.begin();
	while (it != pv_viewsobjs_data.end()) { if ((* it)->refV1 == (ogl_dummy_object *) p1->GetCam()) break; else it++; }
	
	if (it == pv_viewsobjs_data.end())
	{
		assertion_failed(__FILE__, __LINE__, "camera not found.");
	}
	
	custom_camera * ccam = dynamic_cast<custom_camera *>(p1->GetCam());
	if (!ccam)
	{
		assertion_failed(__FILE__, __LINE__, "ccam cast failed.");
	}
	
	pv_viewsobjs_record * vo_rec = new pv_viewsobjs_record;
	
	vo_rec->owner = ccam;
	vo_rec->refV1 = NULL;
	vo_rec->refV2 = p1;
	vo_rec->refO = NULL;
	
	gtk_tree_store_append(pv_viewsobjs_store, & vo_rec->iter, & (* it)->iter);
	pv_viewsobjs_data.push_back(vo_rec);
	
	const char * object_name = "a graphics client ; THIS IS NEVER DISPLAYED?";
	
	gtk_tree_store_set(pv_viewsobjs_store, & vo_rec->iter, 0, object_name, 1, FALSE, -1);
	
	// expand the newly created row...
	
	GtkTreePath * path = gtk_tree_model_get_path(GTK_TREE_MODEL(pv_viewsobjs_store), & vo_rec->iter);
	gtk_tree_view_expand_to_path(GTK_TREE_VIEW(pv_viewsobjs_widget), path);
	gtk_tree_path_free(path);
}

void gtk_app::GraphicsClientRemoved(oglview_wcl * p1)
{
	list<pv_viewsobjs_record *>::iterator it = pv_viewsobjs_data.begin();
	while (it != pv_viewsobjs_data.end()) { if ((* it)->refV2 == (base_wcl *) p1) break; else it++; }
	
	if (it == pv_viewsobjs_data.end())
	{
		assertion_failed(__FILE__, __LINE__, "object not found.");
	}
	
	gtk_tree_store_remove(pv_viewsobjs_store, & (* it)->iter);
	
	pv_viewsobjs_data.erase(it);
	delete (* it);
}

void gtk_app::PlottingClientAdded(base_wcl * p1)
{
//cout << "called gtk_app::PlottingClientAdded()" << endl;
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	pv_viewsobjs_record * vo_rec = new pv_viewsobjs_record;
	
	vo_rec->owner = NULL;
	vo_rec->refV1 = NULL;
	vo_rec->refV2 = p1;
	vo_rec->refO = NULL;
	
	gtk_tree_store_append(pv_viewsobjs_store, & vo_rec->iter, NULL);
	pv_viewsobjs_data.push_back(vo_rec);
	
	const char * object_name = "a plotting client ; THIS IS NEVER DISPLAYED?";
	
	gtk_tree_store_set(pv_viewsobjs_store, & vo_rec->iter, 0, object_name, 1, FALSE, -1);
}

void gtk_app::PlottingClientRemoved(base_wcl * p1)
{
//cout << "called gtk_app::PlottingClientRemoved()" << endl;
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	list<pv_viewsobjs_record *>::iterator it = pv_viewsobjs_data.begin();
	while (it != pv_viewsobjs_data.end()) { if ((* it)->refV2 == (base_wcl *) p1) break; else it++; }
	
	if (it == pv_viewsobjs_data.end())
	{
		assertion_failed(__FILE__, __LINE__, "object not found.");
	}
	
	gtk_tree_store_remove(pv_viewsobjs_store, & (* it)->iter);
	
	pv_viewsobjs_data.erase(it);
	delete (* it);
}

void gtk_app::ObjectAdded(ogl_smart_object * p1)
{
	pv_viewsobjs_record * vo_rec = new pv_viewsobjs_record;
	
	vo_rec->owner = NULL;
	vo_rec->refV1 = NULL;
	vo_rec->refV2 = NULL;
	vo_rec->refO = p1;
	
	gtk_tree_store_append(pv_viewsobjs_store, & vo_rec->iter, NULL);
	pv_viewsobjs_data.push_back(vo_rec);
	
	const char * object_name = p1->GetObjectName();
	
	gtk_tree_store_set(pv_viewsobjs_store, & vo_rec->iter, 0, object_name, 1, FALSE, -1);
}

void gtk_app::ObjectRemoved(ogl_smart_object * p1)
{
	list<pv_viewsobjs_record *>::iterator it = pv_viewsobjs_data.begin();
	while (it != pv_viewsobjs_data.end()) { if ((* it)->refO == p1) break; else it++; }
	
	if (it == pv_viewsobjs_data.end())
	{
		assertion_failed(__FILE__, __LINE__, "object_not_found.");
	}
	
	gtk_tree_store_remove(pv_viewsobjs_store, & (* it)->iter);
	
	pv_viewsobjs_data.erase(it);
	delete (* it);
}

gint gtk_app::ViewsObjsPopupHandler(GtkWidget * widget, GdkEvent * event)
{
	if (project::background_job_running) return TRUE;	// protect the model-data during background jobs...
	
	if (event->type == GDK_BUTTON_PRESS)
	{
		GdkEventButton * event_button = (GdkEventButton *) event;
		if (event_button->button == 3)
		{
			GtkMenu * menu = GTK_MENU(gtk_app::GetAppX()->pv_viewsobjs_menu);
			gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, event_button->button, event_button->time);
			return TRUE;
		}
	}
	
	return FALSE;
}

void gtk_app::viewsobjs_SetCurrent(GtkWidget * widget, gpointer data)
{
	gtk_app * app = gtk_app::GetAppX();
	
// assume that tsel is in mode GTK_SELECTION_SINGLE (as it seems to be); this is the simplest case...
// here the equivalence test of GtkTreeIter is a bit uncertain... need to test any other records???
	
	GtkTreeSelection * tsel = gtk_tree_view_get_selection(GTK_TREE_VIEW(app->pv_viewsobjs_widget));
	GtkTreeModel * tm = gtk_tree_view_get_model(GTK_TREE_VIEW(app->pv_viewsobjs_widget));
	GtkTreeIter iter;
	
	bool has_item = gtk_tree_selection_get_selected(tsel, & tm, & iter);
	if (has_item)
	{
		list<pv_viewsobjs_record *>::iterator it = app->pv_viewsobjs_data.begin();
		while (it != app->pv_viewsobjs_data.end()) { if ((* it)->iter.user_data == iter.user_data) break; else it++; }
		
		if (it == app->pv_viewsobjs_data.end())
		{
			cout << "ERROR : gtk_app::viewsobjs_SetCurrent() failed." << endl;
			exit(EXIT_FAILURE);
		}
		
		bool is_light = ((* it)->refV1 != NULL && dynamic_cast<ogl_light *>((* it)->refV1) != NULL);
		bool is_obj = ((* it)->refO != NULL);
		
		if (is_light)
		{
			gtk_app::GetPrjX()->selected_object = (* it)->refV1;
			
			ostringstream mstr;
			mstr << _("Object ") << gtk_app::GetPrjX()->selected_object->GetObjectName() << _(" is set to current object.") << endl << ends;
			gtk_app::GetPrjX()->PrintToLog(mstr.str().c_str());
		}
		else if (is_obj)
		{
			gtk_app::GetPrjX()->selected_object = (* it)->refO;
			
			ostringstream mstr;
			mstr << _("Object ") << gtk_app::GetPrjX()->selected_object->GetObjectName() << _(" is set to current object.") << endl << ends;
			gtk_app::GetPrjX()->PrintToLog(mstr.str().c_str());
		}
		else gtk_app::GetPrjX()->Message(_("Sorry, this operation is not yet implemented."));
	}
}

void gtk_app::viewsobjs_Delete(GtkWidget *, gpointer data)
{
	gtk_app * app = gtk_app::GetAppX();
	
// assume that tsel is in mode GTK_SELECTION_SINGLE (as it seems to be); this is the simplest case...
// here the equivalence test of GtkTreeIter is a bit uncertain... need to test any other records???
	
	GtkTreeSelection * tsel = gtk_tree_view_get_selection(GTK_TREE_VIEW(app->pv_viewsobjs_widget));
	GtkTreeModel * tm = gtk_tree_view_get_model(GTK_TREE_VIEW(app->pv_viewsobjs_widget));
	GtkTreeIter iter;
	
	bool has_item = gtk_tree_selection_get_selected(tsel, & tm, & iter);
	if (has_item)
	{
		list<pv_viewsobjs_record *>::iterator it = app->pv_viewsobjs_data.begin();
		while (it != app->pv_viewsobjs_data.end()) { if ((* it)->iter.user_data == iter.user_data) break; else it++; }
		
		if (it == app->pv_viewsobjs_data.end())
		{
			assertion_failed(__FILE__, __LINE__, "object not found.");
		}
		
		bool is_light = ((* it)->refV1 != NULL && dynamic_cast<ogl_light *>((* it)->refV1) != NULL);
		bool is_view = ((* it)->refV2 != NULL);
		bool is_obj = ((* it)->refO != NULL);
		
		if (is_light)
		{
			ostringstream mstr;
			mstr << _("Object ") << (* it)->refV1->GetObjectName() << _(" is deleted.") << endl << ends;
			gtk_app::GetPrjX()->PrintToLog(mstr.str().c_str());
			
			if (project::selected_object == (* it)->refV1) project::selected_object = NULL;
			base_app::GetAppB()->RemoveLight((* it)->refV1);
			
			gtk_app::GetPrjX()->UpdateAllGraphicsViews();
		}
		else if (is_view)
		{
			base_wcl * wcl = (* it)->refV2;
			oglview_wcl * oglwcl = dynamic_cast<oglview_wcl *>(wcl);
			
			if (oglwcl != NULL)
			{
				gtk_app::GetPrjX()->RemoveGraphicsClient(oglwcl, false);
			}
			else
			{
				gtk_app::GetPrjX()->RemovePlottingClient(wcl);
			}
		}
		else if (is_obj)
		{
			ostringstream mstr;
			mstr << _("Object ") << (* it)->refO->GetObjectName() << _(" is deleted.") << endl << ends;
			gtk_app::GetPrjX()->PrintToLog(mstr.str().c_str());
			
			if (project::selected_object == (* it)->refO) project::selected_object = NULL;
			gtk_app::GetPrjX()->RemoveObject((* it)->refO);
			
			gtk_app::GetPrjX()->UpdateAllGraphicsViews();
		}
		else gtk_app::GetPrjX()->Message(_("Sorry, this operation is not yet implemented."));
	}
}

void gtk_app::BuildChainsView(void)
{
	if (pv_chains_data.size() != 0)
	{
		ClearChainsView();
	}
	
	if (gtk_app::GetPrjX()->GetCI() != NULL)
	{
		vector<chn_info> & ci_vector = (* gtk_app::GetPrjX()->GetCI());
		
		for (i32u n1 = 0;n1 < ci_vector.size();n1++)
		{
			pv_chains_record * c_rec1 = new pv_chains_record;
			c_rec1->c_r_ind = (n1 << 16) + 0xFFFF; gtk_tree_store_append(pv_chains_store, & c_rec1->iter, NULL);
			
			pv_chains_data.push_back(c_rec1);
			
			ostringstream cis;
			
		//	if (ci_vector[n1].description...	// this is not implemented at the moment...
			
			// show chain index 1,2,3,... to user ; it is 0,1,2,... internally!
			// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			
			cis << _("chain #") << (n1 + 1) << " (";
			
			switch (ci_vector[n1].GetType())
			{
				case chn_info::amino_acid:	cis << "AA"; break;
				case chn_info::nucleic_acid:	cis << "NA"; break;
				default:			cis << "??";
			}
			cis << _(" chain).") << ends;
			
			gtk_tree_store_set(pv_chains_store, & c_rec1->iter, 0, cis.str().c_str(), 1, FALSE, 2, FALSE, 3, FALSE, 4, FALSE, -1);
			
			const char * seq1_buff = ci_vector[n1].GetSequence1();
			for (i32s n2 = 0;n2 < ci_vector[n1].GetLength();n2++)
			{
				pv_chains_record * c_rec2 = new pv_chains_record;
				c_rec2->c_r_ind = (n1 << 16) + n2; gtk_tree_store_append(pv_chains_store, & c_rec2->iter, & c_rec1->iter);
				
				pv_chains_data.push_back(c_rec2);
				
				// show residue index 1,2,3,... to user ; it is 0,1,2,... internally!
				// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
				
				ostringstream rns;
				rns << (n2 + 1) << ends;
				
				char res_id[8] = "?";
				res_id[0] = seq1_buff[n2];
				
				gtk_tree_store_set(pv_chains_store, & c_rec2->iter, 0, FALSE, 1, rns.str().c_str(), 2, res_id, 3, "???", 4, "???", -1);
			}
		}
	}
}

void gtk_app::ClearChainsView(void)
{
	list<pv_chains_record *>::iterator it;
	
	it = pv_chains_data.begin();		// first remove the residue records...
	while (it != pv_chains_data.end())	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	{
		if (((* it)->c_r_ind & 0xFFFF) != 0xFFFF)
		{
			gtk_tree_store_remove(pv_chains_store, & (* it)->iter);
			
			delete (* it);
			pv_chains_data.erase(it);
			
			it = pv_chains_data.begin();
		}
		
		it++;
	}
	
	it = pv_chains_data.begin();		// ...and then the chain records!
	while (it != pv_chains_data.end())	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	{
		gtk_tree_store_remove(pv_chains_store, & (* it)->iter);
		
		delete (* it);
		pv_chains_data.erase(it);
		
		it = pv_chains_data.begin();
	}
}

gint gtk_app::ChainsPopupHandler(GtkWidget * widget, GdkEvent * event)
{
	if (project::background_job_running) return TRUE;	// protect the model-data during background jobs...
	
	if (event->type == GDK_BUTTON_PRESS)
	{
		GdkEventButton * event_button = (GdkEventButton *) event;
		if (event_button->button == 3)
		{
			GtkMenu * menu = GTK_MENU(gtk_app::GetAppX()->pv_chains_menu);
			gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, event_button->button, event_button->time);
			return TRUE;
		}
	}
	
	return FALSE;
}

void gtk_app::chains_UpdateView(GtkWidget *, gpointer data)
{
	gtk_app * app = gtk_app::GetAppX();
	
	// update the data only if it's necessary ; there might be some extra info stored in the chains data!
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	if (gtk_app::GetPrjX()->GetCI() == NULL)
	{
		gtk_app::GetPrjX()->UpdateChains();
	}
}

void gtk_app::chains_SelectItem(GtkWidget *, gpointer data)
{
	gtk_app * app = gtk_app::GetAppX();
	
// assume that tsel is in mode GTK_SELECTION_SINGLE (as it seems to be); this is the simplest case...
// here the equivalence test of GtkTreeIter is a bit uncertain... need to test any other records???
	
	GtkTreeSelection * tsel = gtk_tree_view_get_selection(GTK_TREE_VIEW(app->pv_chains_widget));
	GtkTreeModel * tm = gtk_tree_view_get_model(GTK_TREE_VIEW(app->pv_chains_widget));
	GtkTreeIter iter;
	
	bool has_item = gtk_tree_selection_get_selected(tsel, & tm, & iter);
	if (has_item)
	{
		list<pv_chains_record *>::iterator it = app->pv_chains_data.begin();
		while (it != app->pv_chains_data.end()) { if ((* it)->iter.user_data == iter.user_data) break; else it++; }
		
		if (it == app->pv_chains_data.end())
		{
			assertion_failed(__FILE__, __LINE__, "object not found.");
		}
		
		i32s c_ind = ((* it)->c_r_ind >> 16);
		i32s r_ind = ((* it)->c_r_ind & 0xFFFF);
		
		iter_al c_rng[2]; gtk_app::GetPrjX()->GetRange(1, c_ind, c_rng);
		
		if (r_ind == 0xFFFF)	// select chain
		{
			for (iter_al iter = c_rng[0]; iter != c_rng[1];iter++)
			{
				(* iter).flags |= ATOMFLAG_USER_SELECTED;
			}
		}
		else			// select residue
		{
			iter_al r_rng[2]; gtk_app::GetPrjX()->GetRange(2, c_rng, r_ind, r_rng);
			
			for (iter_al iter = r_rng[0]; iter != r_rng[1];iter++)
			{
				(* iter).flags |= ATOMFLAG_USER_SELECTED;
			}
		}
		
		gtk_app::GetPrjX()->UpdateAllGraphicsViews();
	}
}

void gtk_app::AtomAdded(atom * p1)
{
	pv_atoms_record * a_rec = new pv_atoms_record;
	a_rec->ref = p1; gtk_list_store_append(pv_atoms_store, & a_rec->iter);
	
	pv_atoms_data.push_back(a_rec);
	
	AtomUpdateItem(p1);
}

void gtk_app::AtomUpdateItem(atom * p1)
{
	list<pv_atoms_record *>::iterator it = pv_atoms_data.begin();
	while (it != pv_atoms_data.end()) { if ((* it)->ref == p1) break; else it++; }
	
	if (it == pv_atoms_data.end())
	{
		assertion_failed(__FILE__, __LINE__, "object not found.");
	}
	
	// show atom index 1,2,3,... to user ; it is 0,1,2,... internally!
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	const i32s atmi = (p1->index + 1);
	const char * ele = p1->el.GetSymbol();
	const bool locked = p1->flags & ATOMFLAG_USER_LOCKED;
	
	gtk_list_store_set(pv_atoms_store, & (* it)->iter, 0, atmi, 1, ele, 2, locked, -1);
}

void gtk_app::AtomRemoved(atom * p1)
{
	list<pv_atoms_record *>::iterator it = pv_atoms_data.begin();
	while (it != pv_atoms_data.end()) { if ((* it)->ref == p1) break; else it++; }
	
	if (it == pv_atoms_data.end())
	{
		assertion_failed(__FILE__, __LINE__, "object not found.");
	}
	
	gtk_list_store_remove(pv_atoms_store, & (* it)->iter);
	
	delete (* it);
	pv_atoms_data.erase(it);
}

gint gtk_app::AtomsPopupHandler(GtkWidget * widget, GdkEvent * event)
{
	if (project::background_job_running) return TRUE;	// protect the model-data during background jobs...
	
	if (event->type == GDK_BUTTON_PRESS)
	{
		GdkEventButton * event_button = (GdkEventButton *) event;
		if (event_button->button == 3)
		{
			GtkMenu * menu = GTK_MENU(gtk_app::GetAppX()->pv_atoms_menu);
			gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, event_button->button, event_button->time);
			return TRUE;
		}
	}
	
	return FALSE;
}

void gtk_app::atoms_SelectAtom(GtkWidget *, gpointer data)
{
	gtk_app * app = gtk_app::GetAppX();
	
// assume that tsel is in mode GTK_SELECTION_SINGLE (as it seems to be); this is the simplest case...
// here the equivalence test of GtkTreeIter is a bit uncertain... need to test any other records???
	
	GtkTreeSelection * tsel = gtk_tree_view_get_selection(GTK_TREE_VIEW(app->pv_atoms_widget));
	GtkTreeModel * tm = gtk_tree_view_get_model(GTK_TREE_VIEW(app->pv_atoms_widget));
	GtkTreeIter iter;
	
	bool has_item = gtk_tree_selection_get_selected(tsel, & tm, & iter);
	if (has_item)
	{
		list<pv_atoms_record *>::iterator it = app->pv_atoms_data.begin();
		while (it != app->pv_atoms_data.end()) { if ((* it)->iter.user_data == iter.user_data) break; else it++; }
		
		if (it == app->pv_atoms_data.end())
		{
			assertion_failed(__FILE__, __LINE__, "object not found.");
		}
		
		(* it)->ref->flags ^= ATOMFLAG_USER_SELECTED;
		gtk_app::GetPrjX()->UpdateAllGraphicsViews();
	}
}

gint gtk_app::atoms_ToggleLocked(GtkWidget * widget, gchar * path, gpointer * data)
{
	gtk_app * app = gtk_app::GetAppX();
	
	GtkTreeIter iter;
	if (!gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(data), &iter, path))
	{
		assertion_failed(__FILE__, __LINE__, "iter search failed.");
	}
	
	list<pv_atoms_record *>::iterator it = app->pv_atoms_data.begin();
	while (it != app->pv_atoms_data.end())
	{
		if ((* it)->iter.user_data == iter.user_data) break;
		else it++;
	}
	
	if (it == app->pv_atoms_data.end())
	{
		assertion_failed(__FILE__, __LINE__, "object not found.");
	}
	
	atom * p1 = (* it)->ref;
	p1->SetLocked(!(p1->GetLocked()));
	gtk_list_store_set(GTK_LIST_STORE(data), &iter, 2, p1->GetLocked(), -1);
	
	app->AtomUpdateItem(p1);	// make the list show the updated information...
	
	printf(_("Atom locking changed : %s\n"), p1->GetLocked() ? _("yes") : _("no"));
	return FALSE;
}

void gtk_app::BondAdded(bond * p1)
{
	pv_bonds_record * b_rec = new pv_bonds_record;
	b_rec->ref = p1; gtk_list_store_append(pv_bonds_store, & b_rec->iter);
	
	pv_bonds_data.push_back(b_rec);
	
	BondUpdateItem(p1);
}

void gtk_app::BondUpdateItem(bond * p1)
{
	list<pv_bonds_record *>::iterator it = pv_bonds_data.begin();
	while (it != pv_bonds_data.end()) { if ((* it)->ref == p1) break; else it++; }
	
	if (it == pv_bonds_data.end())
	{
		assertion_failed(__FILE__, __LINE__, "object not found.");
	}
	
	const char * bt_strings[4] = 
	{
		_("Conjugated"),
		_("Single"),
		_("Double"),
		_("Triple")
	};
	
	// show atom index 1,2,3,... to user ; it is 0,1,2,... internally!
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	const i32s atmi1 = (p1->atmr[0]->index + 1);
	const i32s atmi2 = (p1->atmr[1]->index + 1);
	const char * btype = bt_strings[p1->bt.GetValue()];
	
	gtk_list_store_set(pv_bonds_store, & (* it)->iter, 0, atmi1, 1, atmi2, 2, btype, -1);
}

void gtk_app::BondRemoved(bond * p1)
{
	list<pv_bonds_record *>::iterator it = pv_bonds_data.begin();
	while (it != pv_bonds_data.end()) { if ((* it)->ref == p1) break; else it++; }
	
	if (it == pv_bonds_data.end())
	{
		assertion_failed(__FILE__, __LINE__, "object not found.");
	}
	
	gtk_list_store_remove(pv_bonds_store, & (* it)->iter);
	
	delete (* it);
	pv_bonds_data.erase(it);
}

gint gtk_app::BondsPopupHandler(GtkWidget * widget, GdkEvent * event)
{
	if (project::background_job_running) return TRUE;	// protect the model-data during background jobs...
	
	if (event->type == GDK_BUTTON_PRESS)
	{
		GdkEventButton * event_button = (GdkEventButton *) event;
		if (event_button->button == 3)
		{
			GtkMenu * menu = GTK_MENU(gtk_app::GetAppX()->pv_bonds_menu);
			gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, event_button->button, event_button->time);
			return TRUE;
		}
	}
	
	return FALSE;
}

void gtk_app::bonds_SelectBond(GtkWidget *, gpointer data)
{
	gtk_app * app = gtk_app::GetAppX();
	
// assume that tsel is in mode GTK_SELECTION_SINGLE (as it seems to be); this is the simplest case...
// here the equivalence test of GtkTreeIter is a bit uncertain... need to test any other records???
	
	GtkTreeSelection * tsel = gtk_tree_view_get_selection(GTK_TREE_VIEW(app->pv_bonds_widget));
	GtkTreeModel * tm = gtk_tree_view_get_model(GTK_TREE_VIEW(app->pv_bonds_widget));
	GtkTreeIter iter;
	
	bool has_item = gtk_tree_selection_get_selected(tsel, & tm, & iter);
	if (has_item)
	{
		list<pv_bonds_record *>::iterator it = app->pv_bonds_data.begin();
		while (it != app->pv_bonds_data.end()) { if ((* it)->iter.user_data == iter.user_data) break; else it++; }
		
		if (it == app->pv_bonds_data.end())
		{
			assertion_failed(__FILE__, __LINE__, "object not found.");
		}
		
		bool both_selected = (((* it)->ref->atmr[0]->flags & ATOMFLAG_USER_SELECTED) && ((* it)->ref->atmr[1]->flags & ATOMFLAG_USER_SELECTED));
		if (!both_selected)	// select...
		{
			(* it)->ref->atmr[0]->flags |= ATOMFLAG_USER_SELECTED;
			(* it)->ref->atmr[1]->flags |= ATOMFLAG_USER_SELECTED;
		}
		else			// un-select...
		{
			(* it)->ref->atmr[0]->flags &= (~ATOMFLAG_USER_SELECTED);
			(* it)->ref->atmr[1]->flags &= (~ATOMFLAG_USER_SELECTED);
		}
		
		gtk_app::GetPrjX()->UpdateAllGraphicsViews();
	}
}

// the toolbar-button callbacks start here ; the toolbar-button callbacks start here
// the toolbar-button callbacks start here ; the toolbar-button callbacks start here
// the toolbar-button callbacks start here ; the toolbar-button callbacks start here

static int toggle_event_ignore_counter = 0;
void gtk_app::HandleToggleButtons(custom_app::mtool old_mt)
{
	// a static event counter is set up and used so that a cascade of button events is prevented.
	// the same thing could be done by disabling/enabling the callbacks in GTK, but this is simpler.
	
	gboolean state = TRUE;
	if (old_mt != current_mouse_tool) state = FALSE;
	
	if (old_mt == custom_app::mtDraw)
	{
		toggle_event_ignore_counter++;
		gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_draw), state);
	}
	
	if (old_mt == custom_app::mtErase)
	{
		toggle_event_ignore_counter++;
		gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_erase), state);
	}
	
	if (old_mt == custom_app::mtSelect)
	{
		toggle_event_ignore_counter++;
		gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_select), state);
	}
	
	if (old_mt == custom_app::mtZoom)
	{
		toggle_event_ignore_counter++;
		gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_zoom), state);
	}
	
	if (old_mt == custom_app::mtClipping)
	{
		toggle_event_ignore_counter++;
		gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_clipping), state);
	}
	
	if (old_mt == custom_app::mtTranslateXY)
	{
		toggle_event_ignore_counter++;
		gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_translate_xy), state);
	}
	
	if (old_mt == custom_app::mtTranslateZ)
	{
		toggle_event_ignore_counter++;
		gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_translate_z), state);
	}
	
	if (old_mt == custom_app::mtOrbitXY)
	{
		toggle_event_ignore_counter++;
		gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_orbit_xy), state);
	}
	
	if (old_mt == custom_app::mtOrbitZ)
	{
		toggle_event_ignore_counter++;
		gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_orbit_z), state);
	}
	
	if (old_mt == custom_app::mtRotateXY)
	{
		toggle_event_ignore_counter++;
		gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_rotate_xy), state);
	}
	
	if (old_mt == custom_app::mtRotateZ)
	{
		toggle_event_ignore_counter++;
		gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_rotate_z), state);
	}
	
	if (old_mt == custom_app::mtMeasure)
	{
		toggle_event_ignore_counter++;
		gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_measure), state);
	}
}

void gtk_app::maintb_tool_Draw(gpointer p1, guint p2, GtkWidget * p3)
{
	if (toggle_event_ignore_counter > 0)
	{
		cout << "DEBUG : draw skipped ; counter = " << toggle_event_ignore_counter << endl;
		toggle_event_ignore_counter--;
	}
	else
	{
		cout << "DEBUG : draw state is " << gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_draw)) << endl;
		
		custom_app::mtool old_mt = custom_app::current_mouse_tool;
		custom_app::current_mouse_tool = custom_app::mtDraw;
		HandleToggleButtons(old_mt);
	}
}

void gtk_app::maintb_tool_Erase(gpointer, guint, GtkWidget *)
{
	if (toggle_event_ignore_counter > 0)
	{
		cout << "DEBUG : erase skipped ; counter = " << toggle_event_ignore_counter << endl;
		toggle_event_ignore_counter--;
	}
	else
	{
		cout << "DEBUG : erase state is " << gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_erase)) << endl;
		
		custom_app::mtool old_mt = custom_app::current_mouse_tool;
		custom_app::current_mouse_tool = custom_app::mtErase;
		HandleToggleButtons(old_mt);
	}
}

void gtk_app::maintb_tool_Select(gpointer, guint, GtkWidget *)
{
	if (toggle_event_ignore_counter > 0)
	{
		cout << "DEBUG : select skipped ; counter = " << toggle_event_ignore_counter << endl;
		toggle_event_ignore_counter--;
	}
	else
	{
		cout << "DEBUG : select state is " << gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_select)) << endl;
		
		custom_app::mtool old_mt = custom_app::current_mouse_tool;
		custom_app::current_mouse_tool = custom_app::mtSelect;
		HandleToggleButtons(old_mt);
	}
}

void gtk_app::maintb_tool_Zoom(gpointer, guint, GtkWidget *)
{
	if (toggle_event_ignore_counter > 0)
	{
		cout << "DEBUG : zoom skipped ; counter = " << toggle_event_ignore_counter << endl;
		toggle_event_ignore_counter--;
	}
	else
	{
		cout << "DEBUG : zoom state is " << gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_zoom)) << endl;
		
		custom_app::mtool old_mt = custom_app::current_mouse_tool;
		custom_app::current_mouse_tool = custom_app::mtZoom;
		HandleToggleButtons(old_mt);
	}
}

void gtk_app::maintb_tool_Clipping(gpointer, guint, GtkWidget *)
{
	if (toggle_event_ignore_counter > 0)
	{
		cout << "DEBUG : clipping skipped ; counter = " << toggle_event_ignore_counter << endl;
		toggle_event_ignore_counter--;
	}
	else
	{
		cout << "DEBUG : clipping state is " << gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_clipping)) << endl;
		
		custom_app::mtool old_mt = custom_app::current_mouse_tool;
		custom_app::current_mouse_tool = custom_app::mtClipping;
		HandleToggleButtons(old_mt);
	}
}

void gtk_app::maintb_tool_TranslateXY(gpointer, guint, GtkWidget *)
{
	if (toggle_event_ignore_counter > 0)
	{
		cout << "DEBUG : transl_xy skipped ; counter = " << toggle_event_ignore_counter << endl;
		toggle_event_ignore_counter--;
	}
	else
	{
		cout << "DEBUG : transl_xy state is " << gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_translate_xy)) << endl;
		
		custom_app::mtool old_mt = custom_app::current_mouse_tool;
		custom_app::current_mouse_tool = custom_app::mtTranslateXY;
		HandleToggleButtons(old_mt);
	}
}

void gtk_app::maintb_tool_TranslateZ(gpointer, guint, GtkWidget *)
{
	if (toggle_event_ignore_counter > 0)
	{
		cout << "DEBUG : transl_z skipped ; counter = " << toggle_event_ignore_counter << endl;
		toggle_event_ignore_counter--;
	}
	else
	{
		cout << "DEBUG : transl_z state is " << gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_translate_z)) << endl;
		
		custom_app::mtool old_mt = custom_app::current_mouse_tool;
		custom_app::current_mouse_tool = custom_app::mtTranslateZ;
		HandleToggleButtons(old_mt);
	}
}

void gtk_app::maintb_tool_OrbitXY(gpointer, guint, GtkWidget *)
{
	if (toggle_event_ignore_counter > 0)
	{
		cout << "DEBUG : orbit_xy skipped ; counter = " << toggle_event_ignore_counter << endl;
		toggle_event_ignore_counter--;
	}
	else
	{
		cout << "DEBUG : orbit_xy state is " << gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_orbit_xy)) << endl;
		
		custom_app::mtool old_mt = custom_app::current_mouse_tool;
		custom_app::current_mouse_tool = custom_app::mtOrbitXY;
		HandleToggleButtons(old_mt);
	}
}

void gtk_app::maintb_tool_OrbitZ(gpointer, guint, GtkWidget *)
{
	if (toggle_event_ignore_counter > 0)
	{
		cout << "DEBUG : orbit_z skipped ; counter = " << toggle_event_ignore_counter << endl;
		toggle_event_ignore_counter--;
	}
	else
	{
		cout << "DEBUG : orbit_z state is " << gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_orbit_z)) << endl;
		
		custom_app::mtool old_mt = custom_app::current_mouse_tool;
		custom_app::current_mouse_tool = custom_app::mtOrbitZ;
		HandleToggleButtons(old_mt);
	}
}

void gtk_app::maintb_tool_RotateXY(gpointer, guint, GtkWidget *)
{
	if (toggle_event_ignore_counter > 0)
	{
		cout << "DEBUG : rotate_xy skipped ; counter = " << toggle_event_ignore_counter << endl;
		toggle_event_ignore_counter--;
	}
	else
	{
		cout << "DEBUG : rotate_xy state is " << gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_rotate_xy)) << endl;
		
		custom_app::mtool old_mt = custom_app::current_mouse_tool;
		custom_app::current_mouse_tool = custom_app::mtRotateXY;
		HandleToggleButtons(old_mt);
	}
}

void gtk_app::maintb_tool_RotateZ(gpointer, guint, GtkWidget *)
{
	if (toggle_event_ignore_counter > 0)
	{
		cout << "DEBUG : rotate_z skipped ; counter = " << toggle_event_ignore_counter << endl;
		toggle_event_ignore_counter--;
	}
	else
	{
		cout << "DEBUG : rotate_z state is " << gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_rotate_z)) << endl;
		
		custom_app::mtool old_mt = custom_app::current_mouse_tool;
		custom_app::current_mouse_tool = custom_app::mtRotateZ;
		HandleToggleButtons(old_mt);
	}
}

void gtk_app::maintb_tool_Measure(gpointer, guint, GtkWidget *)
{
	if (toggle_event_ignore_counter > 0)
	{
		cout << "DEBUG : measure skipped ; counter = " << toggle_event_ignore_counter << endl;
		toggle_event_ignore_counter--;
	}
	else
	{
		cout << "DEBUG : measure state is " << gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(mtb_mtool_measure)) << endl;
		
		custom_app::mtool old_mt = custom_app::current_mouse_tool;
		custom_app::current_mouse_tool = custom_app::mtMeasure;
		HandleToggleButtons(old_mt);
	}
}

void gtk_app::maintb_dial_Element(gpointer, guint, GtkWidget *)
{
	new gtk_element_dialog();		// will call delete itself...
}

void gtk_app::maintb_dial_BondType(gpointer, guint, GtkWidget *)
{
	new gtk_bondtype_dialog();		// will call delete itself...
}

void gtk_app::maintb_dial_Setup(gpointer, guint, GtkWidget *)
{
	if (project::background_job_running) return;	// protect the model-data during background jobs...
	
	new gtk_setup_dialog(GetPrjX());	// will call delete itself...
}

// the main-menu callbacks start here ; the main-menu callbacks start here
// the main-menu callbacks start here ; the main-menu callbacks start here
// the main-menu callbacks start here ; the main-menu callbacks start here

void gtk_app::mainmenu_FileNew(gpointer, guint, GtkWidget *)
{
	if (project::background_job_running) return;	// protect the model-data during background jobs...
	
	GetAppX()->SetNewProject();
}

void gtk_app::mainmenu_FileOpen(gpointer, guint, GtkWidget *)
{
	if (project::background_job_running) return;	// protect the model-data during background jobs...
	
	new gtk_file_open_dialog(GetPrjX());	// will call delete itself...
}

void gtk_app::mainmenu_FileSaveAs(gpointer, guint, GtkWidget *)
{
	if (project::background_job_running) return;	// protect the model-data during background jobs...
	
	new gtk_file_save_dialog(GetPrjX());	// will call delete itself...
}

void gtk_app::mainmenu_FileClose(gpointer, guint, GtkWidget *)
{
	if (project::background_job_running) return;	// protect the model-data during background jobs...
	
	// gtk_signal_emit_by_name(GTK_OBJECT(main_window), "delete_event");
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	// the above is an "elegant" solution but it segfaults ; why???
	// in below the same stuff is done "manually".
	
	if (!DeleteEventHandler(NULL, NULL, NULL)) gtk_widget_destroy(main_window);
}

void gtk_app::mainmenu_HelpHelp(gpointer, guint, GtkWidget *)
{
	ostringstream str; str << "mozilla ";
	str << project::appdata_path << DIR_SEPARATOR;
	str << project::appversion << DIR_SEPARATOR;
	str << "user-docs/index.html &" << ends;
	
	cout << _("Displaying the User's Manual using the following command:") << endl;
	cout << str.str().c_str() << endl;
	
	system(str.str().c_str());
}

void gtk_app::mainmenu_HelpAbout(gpointer, guint, GtkWidget *)
{
	ostringstream about_str;
	
	about_str << _("Ghemical-") << APPVERSION << _(" released on ") << APPRELEASEDATE << endl;
	about_str << " " << endl;
	about_str << _("For more information please visit:") << endl;
	about_str << WEBSITE << endl;
	about_str << " " << endl;
	
	// leave some lines out to keep the dialog size smaller...
	for (i32s n1 = 0;n1 < 16;n1++) about_str << get_copyright_notice_line(n1) << endl;
	
	about_str << " " << endl;
	about_str << _("Authors:") << endl;
	about_str << "\t\t" << "Tommi Hassinen" << endl;
	about_str << "\t\t" << "Geoff Hutchison" << endl;
	about_str << "\t\t" << "Mike Cruz" << endl;
	about_str << "\t\t" << "Michael Banck" << endl;
	about_str << "\t\t" << "Christopher Rowley" << endl;
	about_str << "\t\t" << "Jean Brefort" << endl;
	about_str << "\t\t" << "Daniel Leidert" << endl;
	about_str << "\t\t" << "Vlado Peshov" << endl;
	
	about_str << ends;
	
	static char about[2048];
	strcpy(about, about_str.str().c_str());
	
	prj->Message(about);
}

/*################################################################################################*/

// eof
