// P1DVIEW_WCL.CPP

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "p1dview_wcl.h"

//#include "project.h"
#include "custom_app.h"

#include <cstring>
#include <sstream>
using namespace std;

/*################################################################################################*/

p1dview_wcl::p1dview_wcl(const char * s1, const char * sv) :
	ac_stor_wcl(new ogl_camera(ogl_ol_static(), 1.0))
{
	cam->ortho = true;
	
	cam->update_vdim = false;
	vdim[0] = (1.0 / 0.90) * 0.5;	// X-scaling ; leave 5% margins...
	vdim[1] = (1.0 / 0.90) * 0.5;	// Y-scaling ; leave 5% margins...
	
	cam->GetLD()->crd[0] = 0.50;	// X-centering
	cam->GetLD()->crd[1] = 0.50;	// Y-centering
	
//const ogl_obj_loc_data * d = cam->GetSafeLD();
//cout << "cam crd = " << d->crd[0] << " " << d->crd[1] << " " << d->crd[2] << endl;
//cout << "cam zdir = { " << d->zdir << " }" << endl << "cam ydir = { " << d->ydir << " }" << endl;
	
	if (!s1 || !strlen(s1)) assertion_failed(__FILE__, __LINE__, "invalid s1.");
	else { name1 = new char[strlen(s1) + 1]; strcpy(name1, s1); }
	
	if (!sv || !strlen(sv)) assertion_failed(__FILE__, __LINE__, "invalid sv.");
	else { namev = new char[strlen(sv) + 1]; strcpy(namev, sv); }
}

p1dview_wcl::~p1dview_wcl(void)
{
	delete[] name1;
	delete[] namev;
	
	// problem : lifetime of the camera object needs to be longer than
	// lifetime of this object since it is needed at the base class dtor.
	// solution : ask the base class to do the cleanup work for us...
	
	delete_cam_plz = true;
}

void p1dview_wcl::AddData(double c1, double v)
{
	p1d_data newdata;
	newdata.ac_i = StoreAC(NULL);
	
	newdata.c1 = c1;
	newdata.v = v;
	
	dv.push_back(newdata);
}

void p1dview_wcl::AddDataWithAC(double c1, double v, engine * eng)
{
	p1d_data newdata;
	newdata.ac_i = StoreAC(eng);
	
	newdata.c1 = c1;
	newdata.v = v;
	
	dv.push_back(newdata);
}

void p1dview_wcl::AddDataWithAC(double c1, double v, model * mdl, int cset)
{
	p1d_data newdata;
	newdata.ac_i = StoreAC(mdl, cset);
	
	newdata.c1 = c1;
	newdata.v = v;
	
	dv.push_back(newdata);
}

void p1dview_wcl::Finalize(void)
{
	if (dv.empty()) return;
	
	min1 = max1 = dv[0].c1;
	minv = maxv = dv[0].v;
	
	for (i32u n1 = 1;n1 < dv.size();n1++)
	{
		if (dv[n1].c1 < min1) min1 = dv[n1].c1;
		if (dv[n1].c1 > max1) max1 = dv[n1].c1;
		
		if (dv[n1].v < minv) minv = dv[n1].v;
		if (dv[n1].v > maxv) maxv = dv[n1].v;
	}
}

void p1dview_wcl::ButtonEvent(int x, int y)
{
	base_wnd * wnd = GetWnd();
	project * prj = custom_app::GetAppC()->GetPrj();
	
	if (!(wnd->GetWidth() > 1)) return;
	fGL sx = 1.10 * (fGL) x / (fGL) wnd->GetWidth() - 0.05;
	if (sx < 0.0) return; if (sx > 1.0) return;
	
	fGL sel1 = sx * (max1 - min1);
	
	int index = NOT_DEFINED;
	fGL mindiff = max1 - min1;
	for (i32u n1 = 0;n1 < dv.size();n1++)
	{
		fGL diff = fabs(sel1 - dv[n1].c1);
		if (diff < mindiff)
		{
			index = n1;
			mindiff = diff;
		}
	}
	
	if (index < 0 || index >= (int) dv.size()) return;
	
	// ok, we have a valid selection ; show the data!!!
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	ostringstream str1;
	str1 << name1 << " = " << dv[index].c1 << " " << namev << " = " << dv[index].v << endl << ends;
	prj->PrintToLog(str1.str().c_str());
	
	if (dv[index].ac_i != NOT_DEFINED) ShowAC(dv[index].ac_i);
}

void p1dview_wcl::MotionEvent(int x, int y)
{
	ButtonEvent(x, y);
}

void p1dview_wcl::UpdateWnd(void)
{
	base_wnd * wnd = GetWnd();
	if (!wnd || wnd->GetWidth() < 0 || !cam) return;
	
	wnd->SetCurrent();
	cam->RenderScene(wnd, false, false);
}

void p1dview_wcl::InitGL(void)
{
	// all classes that inherit pangofont_wcl must call ogl_InitPangoFont()!!!
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	ogl_InitPangoFont("courier 12");
}

void p1dview_wcl::RenderGL(rmode)
{
	if (dv.empty()) return;
	
	glInitNames();
	
// 20061024 ; it seems that the X-coordinate must be inverted...
// origo of the coordinate system is at the lower-left corner.
	
	for (int n1 = 0;n1 < ((int) dv.size()) - 1;n1++)
	{
		glColor3f(0.0, 1.0, 0.0);	// green
		
		fGL x; fGL y;
		glBegin(GL_LINES);
		
		x = 1.0 - (dv[n1 + 0].c1 - min1) / (max1 - min1);	// invX
		y = (dv[n1 + 0].v - minv) / (maxv - minv);
		glVertex3f(x, y, 0.1);
		
		x = 1.0 - (dv[n1 + 1].c1 - min1) / (max1 - min1);	// invX
		y = (dv[n1 + 1].v - minv) / (maxv - minv);
		glVertex3f(x, y, 0.1);
		
		glEnd();
	}
}

/*################################################################################################*/

// eof
