// OGL_RIBBON.H : ribbon models for peptides/proteins.

// Copyright (C) 1998 Tommi Hassinen, Jarno Huuskonen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#ifndef OGL_RIBBON_H
#define OGL_RIBBON_H

//#include "ghemicalconfig2.h"

class ogl_ribbon;

/*################################################################################################*/

class spline;	 	// spline.h

#include "project.h"

//#include "ogl_objects.h"
#include <oglappth/ogl_camera.h>
#include <oglappth/transparent.h>

#include <ghemical/typedef.h>

/*################################################################################################*/

//#define RIBBON_USE_DISPLSTS	// un-comment this to enable use of OpenGL display lists...
//^^^^^^^^^^^^^^^^^^^^^^^^^^^ 20090420 ; this seems to have problems : assertion `GTK_WIDGET_REALIZED (widget)' failed

class ogl_ribbon :
	public ogl_smart_object
{
	public:
	
	project * prj;
	color_mode * cmode;
	
	i32s extra_points;
	i32s chn; i32s length;
	
	fGL * cp1;		// control points (three components)
	fGL * cp2;		// control points (three components)
	
	spline * ref1;
	fGL * head_refs1[2];
	fGL * head_points1;	// (three components)
	fGL * tail_refs1[2];
	fGL * tail_points1;	// (three components)
	
	spline * ref2;
	fGL * head_refs2[2];
	fGL * head_points2;	// (three components)
	fGL * tail_refs2[2];
	fGL * tail_points2;	// (three components)
	
	fGL * data1;		// places of the control points in spline
	fGL * data2a;		// coordinates calculated using the spline (three components)
	fGL * data2b;		// coordinates calculated using the spline (three components)
	fGL * data3;		// colors of the points (three components)
	
#ifdef RIBBON_USE_DISPLSTS
	iGLu list_id;
#endif	// RIBBON_USE_DISPLSTS
	
	static const i32s resol;
	
	static const fGL width;
	static const fGL helix;
	
	public:
	
	ogl_ribbon(project *, color_mode *, i32s, i32s);
	~ogl_ribbon(void);
	
	void UpdateExtraPoints(fGL **, fGL *, fGL **, fGL *);
	
	const char * GetObjectName(void) { return "ribbon"; }	// virtual
	
	void CameraEvent(const ogl_camera &) { }	// virtual
	
	bool BeginTransformation(void) { return false; }	// virtual
	bool EndTransformation(void) { return false; }		// virtual
	
	void Render(void);		// virtual
};

/*################################################################################################*/

#endif	// OGL_RIBBON_H

// eof
