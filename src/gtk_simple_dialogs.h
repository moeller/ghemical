// GTK_SIMPLE_DIALOGS.H : the GTK-dialogs not using GLADE/LIBGLADE.

// Copyright (C) 1999 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#ifndef GTK_SIMPLE_DIALOGS_H
#define GTK_SIMPLE_DIALOGS_H

//#include "ghemicalconfig2.h"

class gtk_project;	// gtk_project.h

#include <gtk/gtk.h>

#include "gtk_app.h"

// the dialogs here are made modal, so that only one dialog of it's kind can exist at any
// moment. therefore GtkWidgets are made static, because there is no need to store several
// GtkWidgets. if modal dialogs are needed, an array/container of GtkWidgets are needed
// in a similar way as in window classes...

/*################################################################################################*/

class gtk_element_dialog
{
	private:
	
	static gtk_element_dialog * current_object;
	
	static GtkWidget * dialog;
	static GtkWidget * current_element_widget;
	
	public:
	
	gtk_element_dialog(void);
	~gtk_element_dialog(void);
	
	static void SignalHandler(GtkWidget *, int);
};

/*################################################################################################*/

class gtk_bondtype_dialog
{
	private:
	
	static gtk_bondtype_dialog * current_object;
	
	static GtkWidget * dialog;
	static GtkWidget * current_bond_widget;	

	public:
	
	gtk_bondtype_dialog(void);
	~gtk_bondtype_dialog(void);
	
	static void SignalHandler(GtkWidget *, int);
};

/*################################################################################################*/

const char * make_tmp_filename(gtk_project *);

class gtk_file_dialog
{
	protected:
	
	static GtkWidget * dialog;
	
	public:
	
	gtk_file_dialog(const char *, const char *, const char *);
	virtual ~gtk_file_dialog(void);
	
	static void DestroyHandler(GtkWidget *, gpointer);
	
	static void OkButtonHandler(GtkWidget *, gpointer);
	static void CancelButtonHandler(GtkWidget *, gpointer);
	
	virtual bool OkEvent(const char *) = 0;
	virtual void CancelEvent(void) = 0;
};

/*################################################################################################*/

class gtk_file_open_dialog : public gtk_file_dialog
{
	private:
	
	static const char title[];
	static gtk_project * prj;
	bool insert;
	
	public:
	
	gtk_file_open_dialog(gtk_project *);
	~gtk_file_open_dialog(void);
	
	bool OkEvent(const char *);	// virtual
	void CancelEvent(void);		// virtual
};

/*################################################################################################*/

class gtk_file_save_dialog : public gtk_file_dialog
{
	private:
	
	static const char title[];
	static gtk_project * prj;
	
	public:
	
	gtk_file_save_dialog(gtk_project *);
	~gtk_file_save_dialog(void);
	
	bool OkEvent(const char *);	// virtual
	void CancelEvent(void);		// virtual
};

/*################################################################################################*/

class gtk_file_save_graphics_dialog : public gtk_file_dialog
{
	private:
	
	static const char title[];
	static gtk_project * prj;
	
	public:
	
	gtk_file_save_graphics_dialog(gtk_project *);
	~gtk_file_save_graphics_dialog(void);
	
	bool OkEvent(const char *);	// virtual
	void CancelEvent(void);		// virtual
};

/*################################################################################################*/

class gtk_trajfile_dialog : public gtk_file_dialog
{
	private:
	
	static const char title[];
	static gtk_project * prj;
	
	public:
	
	gtk_trajfile_dialog(gtk_project *);
	~gtk_trajfile_dialog(void);
	
	bool OkEvent(const char *);	// virtual
	void CancelEvent(void);		// virtual
};

/*################################################################################################*/

class gtk_importpdb_dialog : public gtk_file_dialog
{
	private:
	
	static const char title[];
	static gtk_project * prj;
	
	public:
	
	gtk_importpdb_dialog(gtk_project *);
	~gtk_importpdb_dialog(void);
	
	bool OkEvent(const char *);	// virtual
	void CancelEvent(void);		// virtual
};

/*################################################################################################*/

class gtk_command_dialog
{
	private:
	
	static gtk_project * prj;
	static oglview_wcl * wcl;
	
	static GtkWidget * dialog;
	static GtkWidget * entry;
	
	public:
	
	gtk_command_dialog(gtk_project *, oglview_wcl *, const char *);
	virtual ~gtk_command_dialog(void);
	
	static void DestroyHandler(GtkWidget *, gpointer);
	
	static void OkButtonHandler(GtkWidget *, gpointer);
	static void CancelButtonHandler(GtkWidget *, gpointer);
};

/*################################################################################################*/

#endif	// GTK_SIMPLE_DIALOGS_H

// eof
